package osw.cage.util

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.edit
import androidx.preference.PreferenceManager

object PrefUtil {

    lateinit var sharedPreferences: SharedPreferences

    val PK_NIGHTMODE = "pk_night_mode"
    val PK_DMPASSWORD = "pk_dm_password"
    val PK_STARTPG = "pk_start_page"

    fun initialize(context: Context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun isNightMode(): Boolean {
        return sharedPreferences.getBoolean(PK_NIGHTMODE, false)
    }

    fun getNightmode(): Int {
        if (isNightMode()) {
            return AppCompatDelegate.MODE_NIGHT_YES
        } else {
            return AppCompatDelegate.MODE_NIGHT_NO
        }
    }

    fun getDmPassword(): String? {
        return sharedPreferences.getString(PK_DMPASSWORD, "")
    }

    fun setDmPassword(pass: String) {
        sharedPreferences.edit {
            this.putString(PK_DMPASSWORD, pass )
            this.commit()
        }
    }

    fun getStartingPage(): String? {
        return sharedPreferences.getString(PK_STARTPG, "Favourites")
    }
}