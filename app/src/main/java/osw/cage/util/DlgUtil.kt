package osw.cage.util

import android.app.Activity
import android.content.Context
import android.net.Uri
import android.text.InputType
import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.afollestad.materialdialogs.LayoutMode
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.bottomsheets.BottomSheet
import com.afollestad.materialdialogs.checkbox.checkBoxPrompt
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.datetime.timePicker
import com.afollestad.materialdialogs.files.FileFilter
import com.afollestad.materialdialogs.files.fileChooser
import com.afollestad.materialdialogs.files.folderChooser
import com.afollestad.materialdialogs.input.input
import com.afollestad.materialdialogs.list.listItems
import com.afollestad.materialdialogs.list.listItemsSingleChoice
import com.barisatalay.filterdialog.FilterDialog
import com.barisatalay.filterdialog.model.DialogListener
import osw.cage.ui.ref.JsonDataType
import osw.cage.ui.ref.RefParser
import java.io.File
import java.util.*

class RefClient(val internal: Boolean = false) : WebViewClient() {
    override fun shouldOverrideUrlLoading(
        view: WebView,
        url: String
    ): Boolean {
        val request: Uri = Uri.parse(url)
        var tp = JsonDataType.RULES
        when (request.scheme) {
            "item" -> {
                tp = JsonDataType.ITEM
            }
            "condition" -> {
                tp = JsonDataType.CONDITION
            }
            "spell" -> {
                tp = JsonDataType.SPELL
            }
            "creature" -> {
                tp = JsonDataType.BEAST
            }
            "background" -> {
                tp = JsonDataType.BACKGROUND
            }
            "class" -> {
                tp = JsonDataType.CLASS
            }
        }
        if (internal) {
            view.loadDataWithBaseURL(
                "",
                RefParser.findInfo(view.context, request.authority!!, tp),
                "text/html",
                "utf-8",
                ""
            )
        } else {
            DlgUtil.webinfo(
                view.context,
                "",
                RefParser.findInfo(view.context, request.authority!!, tp)
            )
        }
        return true
    }
}

object DlgUtil {

    fun confirm(
        context: Context,
        title: String, message: String,
        func: () -> Unit, cancelfunc: (() -> Unit)? = null
    ) {
        MaterialDialog(context)
            .title(text = title)
            .message(text = message)
            .positiveButton(text = "Ok") {
                func()
            }
            .negativeButton(text = "Cancel") {
                if (cancelfunc != null) {
                    cancelfunc()
                }
            }
            .show()
    }

    fun info(context: Context, infoTitle: String, infoMessage: String) {
        MaterialDialog(context).show {
            title(text = infoTitle)
            message(text = infoMessage)
        }
    }

    fun webinfo(context: Context, infoTitle: String, infoMessage: String) {
        val layout = LinearLayout(context)
        layout.setPadding(5, 5, 5, 5)
        val webview = WebView(context)
        webview.webViewClient = RefClient(true)
        webview.loadDataWithBaseURL("", infoMessage, "text/html", "UTF-8", "")
        layout.addView(webview)
        MaterialDialog(context)
            .customView(view = layout, scrollable = true)
            .show {
                if (infoTitle.isNotEmpty()) {
                    title(text = infoTitle)
                }
            }
    }

    fun input(
        context: Context,
        title: String, message: String,
        defaultVal: String, func: (text: String?) -> Unit
    ) {
        val dlg = MaterialDialog(context)
        dlg.title(text = title)
        dlg.message(text = message)
        dlg.input(prefill = defaultVal) { _, charSequence ->
            func(charSequence.toString())
        }.negativeButton(text = "Cancel")
            .show()
    }

    fun inputPass(
        context: Context,
        title: String, message: String,
        showRemember: Boolean,
        func: (text: String?, remember: Boolean) -> Unit
    ) {
        val dlg = MaterialDialog(context)
        var remember = false
        dlg.title(text = title)
        dlg.message(text = message)
        dlg.input(inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
        { _, charSequence ->
            func(charSequence.toString(), remember)
        }.negativeButton(text = "Cancel")
        if (showRemember) {
            dlg.checkBoxPrompt(text = "Save passphrase") {
                remember = it
            }
        }
        dlg.show()
    }

    fun inputInt(
        context: Context,
        title: String, message: String,
        defValue: Int? = null,
        func: (Int) -> Unit
    ) {
        val dlg = MaterialDialog(context)
        dlg.title(text = title)
        dlg.message(text = message)
        dlg.input(
            prefill = defValue?.toString(),
            inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_NUMBER_FLAG_SIGNED
        )
        { _, charSequence ->
            func(charSequence.toString().toInt())
        }.negativeButton(text = "Cancel")
        dlg.show()
    }

    fun doubleinput(
        context: Context,
        title: String,
        msgOne: String, msgTwo: String,
        defOne: String, defTwo: String,
        typeOne: Int = InputType.TYPE_CLASS_TEXT, typeTwo: Int = InputType.TYPE_CLASS_TEXT,
        func: (String, String) -> Unit
    ) {
        val layout = LinearLayout(context)
        layout.orientation = LinearLayout.VERTICAL
        layout.setPadding(10, 10, 10, 10)
        val editOne = EditText(context)
        editOne.setText(defOne)
        editOne.inputType = typeOne
        val labelOne = TextView(context)
        labelOne.text = msgOne
        val editTwo = EditText(context)
        editTwo.setText(defTwo)
        editTwo.inputType = typeTwo
        val labelTwo = TextView(context)
        labelTwo.text = msgTwo
        layout.addView(labelOne)
        layout.addView(editOne)
        layout.addView(labelTwo)
        layout.addView(editTwo)
//        aview.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
//        aview.layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT
        MaterialDialog(context)
            .title(text = title)
            .customView(view = layout)
            .positiveButton {
                func(editOne.text.toString(), editTwo.text.toString())
            }
            .negativeButton { }
            .show()
    }

    fun selectFile(
        context: Context,
        initalDir: File,
        filter: FileFilter = null,
        func: (File) -> Unit
    ) {
        if (!initalDir.exists()) {
            initalDir.mkdirs()
        }
        MaterialDialog(context)
            .negativeButton(text = "Cancel")
            .fileChooser(initalDir, filter = filter) { _, file ->
                func(file)
            }.show()
    }

    fun selectFolder(
        context: Context,
        initalDir: File,
        filter: FileFilter = null,
        func: (File) -> Unit
    ) {
        Log.i("SS", "select folder ${initalDir.toString()}")
        MaterialDialog(context)
            .negativeButton(text = "Cancel")
            .folderChooser(initalDir, filter = filter) { _, file ->
                func(file)
            }.show()
    }

    fun select(
        context: Context,
        title: String,
        items: List<String>,
        initial: Int,
        func: (Int, String) -> Unit
    ) {
        if (title.isBlank()) {
            MaterialDialog(context)
                .listItemsSingleChoice(
                    items = items,
                    initialSelection = initial
                ) { _, index, text ->
                    func(index, text as String)
                }
                .show()
        } else {
            MaterialDialog(context)
                .title(text = title)
                .listItemsSingleChoice(
                    items = items,
                    initialSelection = initial
                ) { _, index, text ->
                    func(index, text as String)
                }
                .show()
        }
    }

    fun selectBs(
        context: Context,
        title: String,
        items: List<String>,
        func: (Int, String) -> Unit
    ) {
        if (title.isBlank()) {
            MaterialDialog(context, BottomSheet(LayoutMode.WRAP_CONTENT))
                .listItems(
                    items = items
                ) { _, index, text ->
                    func(index, text as String)
                }
                .show()
        } else {
            MaterialDialog(context, BottomSheet(LayoutMode.WRAP_CONTENT))
                .title(text = title)
                .listItems(
                    items = items
                ) { _, index, text ->
                    func(index, text as String)
                }
                .show()
        }
    }

    fun selectWithSearch(
        activity: Activity,
        title: String,
        items: List<String>,
        initial: Int,
        func: (Int, String) -> Unit
    ) {
        val filterDialog = FilterDialog(activity)
        filterDialog.toolbarTitle = title
        filterDialog.searchBoxHint = "..."
        filterDialog.setList(items)
        filterDialog.backPressedEnabled(false)
        filterDialog.setOnCloseListener { filterDialog.dispose() }
        filterDialog.show(DialogListener.Single { selectedItem ->
            filterDialog.dispose()
            func(items.indexOf(selectedItem.name), selectedItem.name)
        })
    }


    fun time(context: Context, title: String, func: (Calendar) -> Unit) {
        MaterialDialog(context)
            .title(text = title)
            .timePicker(requireFutureTime = true) { _, dateTime ->
                func(dateTime)
            }
            .show()
    }


}