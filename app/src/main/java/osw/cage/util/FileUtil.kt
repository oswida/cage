package osw.cage.util

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import android.provider.OpenableColumns
import java.io.*

object FileUtil {

    val AppRoot = "Cage"

    fun assetFile(context: Context, filename: String): String {
        var retv = ""
        context.assets.open(filename).apply {
            retv = this.readBytes().toString(Charsets.UTF_8)
        }.close()
        return retv
    }

    fun appFile(context: Context?, path: String): File {
        var parts = path.split(File.separator).toMutableList()
        val filename = parts.last()
        parts = parts.dropLast(1).toMutableList()
        parts.add(AppRoot)
        val dir = parts.joinToString(File.separator)
        val target = context!!.getExternalFilesDir(dir)!!
        if (!target.exists()) {
            target.mkdirs()
        }
        return target.resolve(filename)
    }

    fun copyFile(context: Context?, sourceuri: Uri, destpath: String) {
        val target = appFile(context, destpath)
        if (!target.exists()) {
            target.createNewFile()
        }
        var bis: InputStream? = null
        var bos: OutputStream? = null

        try {
            bis = context?.contentResolver!!.openInputStream(sourceuri)!!
            bos = FileOutputStream(target, false)
            val buf = bis.readBytes()
            bos.write(buf)
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            try {
                bis?.close()
                bos?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    fun deleteFile(context: Context?, path: String) {
        val target = appFile(context, path)
        if (target.exists()) {
            target.delete()
        }
    }

    fun extractFilename(resolver: ContentResolver, uri: Uri): String {
        val returnCursor = resolver.query(uri, null, null, null, null)!!
        val nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        returnCursor.moveToFirst()
        val name = returnCursor.getString(nameIndex)
        returnCursor.close()
        return name
    }

}