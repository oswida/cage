package osw.cage.ui.map

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import osw.cage.R

open class MapItem(
    var name: String? = null,
    var description: String? = null,
    var filename: String? = null,
    var locations: String? = null,
    var dm: Boolean = false
) : AbstractItem<MapItem.ViewHolder>() {

    override val type: Int
        get() = R.id.maplist

    override val layoutRes: Int
        get() = R.layout.list_item

    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v)
    }

    class ViewHolder(view: View) : FastAdapter.ViewHolder<MapItem>(view) {
        var name: TextView = view.findViewById(R.id.title)
        var description: TextView = view.findViewById(R.id.subtitle)
        var icon: ImageView = view.findViewById(R.id.icon)

        override fun bindView(item: MapItem, payloads: MutableList<Any>) {
            name.text = item.name
            description.text = item.description
            if (item.dm) {
                icon.setImageResource(R.drawable.ic_password)
            }
        }

        override fun unbindView(item: MapItem) {
            name.text = null
            description.text = null
        }
    }
}

class MapViewModel : ViewModel() {

    private val _list = MutableLiveData<List<MapItem>>().apply {
        value = listOf(
            MapItem(
                "Sigil Map", "Interactive Sigil map",
                "img/Sigil_Map.jpg", "json/Sigil_Map.json"
            ),
            MapItem(
                "Outlands", "Outlands map",
                "img/Outlands.jpg"
            ),
            MapItem(
                "The Great Ring", "Great ring of outer planes",
                "img/Great_Ring.jpg"
            ),
            MapItem(
                "Planes Schema", "Planes schematics",
                "img/Planes_Schema.jpg"
            ),
            MapItem(
                "Twelve Factols Inn", "Places in Sigil (DM)",
                "img/Twelve_Factols.jpg"
            ),
            MapItem(
                "Fortunes Wheel", "Places in Sigil (DM)",
                "img/Fortunes_Wheel.jpg"
            ),
            MapItem(
                "Temple of Abyss", "Places in Sigil (DM)",
                "img/Temple_of_Abyss.jpg",null,true
            ),
            MapItem(
                "Hall of Information", "Places in Sigil (DM)",
                "img/Hall_of_Information.jpg",null,true
            ),
            MapItem(
                "Chirper's", "Places in Sigil (DM)",
                "img/Chirpers.jpg"
            ),
            MapItem(
                "The Bottle & Jug", "Places in Sigil (DM)",
                "img/Bottle_and_Jug.jpg"
            ),
            MapItem(
                "Weary Spirit Infirmary", "Places in Sigil (DM)",
                "img/Weary_Spirit_Infirmary.jpg"
            )
        )
    }
    val list: LiveData<List<MapItem>> = _list

}
