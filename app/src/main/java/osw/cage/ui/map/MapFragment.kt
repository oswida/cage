package osw.cage.ui.map

import android.graphics.PointF
import android.os.Bundle
import android.text.Html
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.davemorrissey.labs.subscaleview.ImageSource
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import osw.cage.R
import osw.cage.db.DbWorker
import osw.cage.db.MapBookmark
import osw.cage.util.DlgUtil
import osw.cage.util.FileUtil
import java.lang.reflect.Type
import kotlin.math.pow
import kotlin.math.roundToInt
import kotlin.math.sqrt

class MapFragment(val filename: String, val points: String? = null) : Fragment() {

    lateinit var imageView: PinView
    val locations = mutableMapOf<String, JsonObject>()
    var mapWidth: Int = 0
    var mapHeight: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_map, container, false)
        imageView = view.findViewById(R.id.imageView)
        imageView.setImage(ImageSource.asset(filename))
        imageView.setOnTouchListener { v, event ->
            if (event.action == 0) {
                imageView.lastPoint.set(event.x, event.y)
            }
            false
        }
        imageView.setOnLongClickListener {

            var lp = PointF()
            imageView.viewToSourceCoord(imageView.lastPoint, lp)
            var lst = findLocationsInArea(lp, 100)
            if (lst.isNotEmpty()) {
                DlgUtil.selectBs(
                    context!!,
                    "Nearby points",
                    lst.map { it.get("title").asString }) { pos, item ->
                    if (item != null) {
                        val pt = locations[item]!!
                        moveMap(pt, false)
                        DlgUtil.info(
                            context!!,
                            pt.get("title").asString,
                            Html.fromHtml(pt.get("description").asString).toString()
                        )
                        imageView.tempPointList.add(pt)
                    }
                }
                true
            }
            false
        }
        setHasOptionsMenu(true)
        if (points != null) {
            loadPoints()
            reloadBookmarks()
        }
        return view
    }

    private fun findLocationsInArea(pt: PointF, radius: Int): List<JsonObject> {
        val retv = mutableListOf<JsonObject>()
        locations.values.forEach {
            val posx = it.get("posx").asFloat
            val posy = it.get("posy").asFloat
            val len = sqrt(
                (posx.toDouble() - pt.x.toDouble()).pow(2.0) + (posy.toDouble() - pt.y.toDouble()).pow(
                    2.0
                )
            )
            if (len <= radius) {
                retv.add(it)
            }
        }
        return retv
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.map_menu, menu)
    }

    private fun reloadBookmarks() {
        imageView.pointList.clear()
        DbWorker.postDbTask(context) { db ->
            var lst = db.mapBookmarkDao().getAll(filename)
            lst.forEach {
                val tt = locations[it.name]
                if (tt != null) {
                    imageView.pointList.add(tt)
                }
            }
        }
    }

    private fun loadPoints() {
        locations.clear()
        val data = FileUtil.assetFile(context!!, points!!)
        val objType: Type = object : TypeToken<JsonObject?>() {}.type
        val obj: JsonObject = Gson().fromJson(data, objType)
        if (obj.has("mapwidth")) {
            mapWidth = obj.get("mapwidth").asInt
        }
        if (obj.has("mapheight")) {
            mapHeight = obj.get("mapheight").asInt
        }
        if (obj.has("levels")) {
            val pts = obj.get("levels").asJsonArray[0].asJsonObject.get("locations").asJsonArray
            pts.forEach { point ->
                val posx = (point.asJsonObject.get("x").asDouble * mapWidth).roundToInt()
                val posy = (point.asJsonObject.get("y").asDouble * mapHeight).roundToInt()
                point.asJsonObject.addProperty("posx", posx)
                point.asJsonObject.addProperty("posy", posy)
                locations[point.asJsonObject.get("title").asString] = point.asJsonObject
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_maplist -> {
                val tr = fragmentManager!!.beginTransaction()
                tr.replace(R.id.nav_host_fragment, MapListFragment())
                tr.addToBackStack(null)
                tr.commit()
                return true
            }
            R.id.action_add_point -> {
                if (locations.isNotEmpty()) {
                    DlgUtil.selectWithSearch(
                        activity!!, "Locations",
                        locations.values.map { it.get("title").asString }, 0
                    ) { _, item ->
                        val pt = locations[item]
                        Snackbar.make(view!!, item, Snackbar.LENGTH_LONG).show()
                        if (!imageView.pointList.contains(pt)) {
                            imageView.pointList.add(pt!!)
                            DbWorker.postDbTask(context!!) { db ->
                                db.mapBookmarkDao().insertAll(
                                    MapBookmark(
                                        null,
                                        filename,
                                        pt.get("title").asString,
                                        pt.toString()
                                    )
                                )
                                Toast.makeText(context,
                                    "Point ${pt.get("title").asString} saved", Toast.LENGTH_SHORT).show()
                            }
                        }
                        moveMap(pt!!)
                    }
                }
            }
            R.id.action_toc -> {
                if (imageView.pointList.size > 0) {
                    DlgUtil.selectBs(context!!, "Select point",
                        imageView.pointList.map { it.get("title").asString }
                    ) { _, result ->
                        val pt = locations[result]
                        moveMap(pt!!)
                    }
                }
            }
            R.id.action_clear -> {
                if (imageView.pointList.size > 0) {
                    DlgUtil.confirm(context!!, "Clear points", "Please confirm points clear", {
                        imageView.pointList.clear()
                        imageView.setScaleAndCenter(1.0f, PointF(mapWidth / 2.0f, mapHeight / 2.0f))
                        DbWorker.postDbTask(context) { db ->
                            db.mapBookmarkDao().clear()
                            Toast.makeText(context,
                                "Points cleared", Toast.LENGTH_SHORT).show()
                        }
                    }, null)
                }
            }
            R.id.action_del_point -> {
                if (imageView.pointList.size > 0) {
                    DlgUtil.selectBs(context!!, "Delete point",
                        imageView.pointList.map { it.get("title").asString }
                    ) { _, result ->
                        val pt = locations[result]
                        imageView.pointList.remove(pt)
                        DbWorker.postDbTask(context) { db ->
                            val tt =
                                db.mapBookmarkDao().getForName(filename, pt!!.get("title").asString)
                            db.mapBookmarkDao().delete(tt)
                            Toast.makeText(context,
                                "Point ${pt.get("title").asString} deleted", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun moveMap(point: JsonObject, rescale: Boolean = true) {
        val posx = point.get("posx").asInt
        val posy = point.get("posy").asInt
        val target = PointF(posx.toFloat(), posy.toFloat())
        var sc = imageView.scale
        if (rescale) {
            sc = 1.0f
        }
        imageView.setScaleAndCenter(sc, target)
    }
}