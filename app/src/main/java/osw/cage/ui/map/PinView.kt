package osw.cage.ui.map

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.PointF
import android.util.AttributeSet
import android.util.Log
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView
import com.google.gson.JsonObject


class PinView(context: Context?, attr: AttributeSet?) :
    SubsamplingScaleImageView(context, attr) {

    private val paint: Paint = Paint()
    val pointList = mutableListOf<JsonObject>()
    val tempPointList = mutableListOf<JsonObject>()
    private val sCenter = PointF()
    private val vCenter = PointF()
    private var strokeWidth = 0
    var lastPoint = PointF(0.0f,0.0f)

    constructor(context: Context?) : this(context, null)

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (!isReady) {
            return
        }

        paint.textSize = 20 * scale
        val radius = scale * 25f

        pointList.forEach {
            sCenter.set(it.get("posx").asFloat, it.get("posy").asFloat)
            sourceToViewCoord(sCenter, vCenter)
            paint.strokeWidth = strokeWidth * 2.toFloat()
            paint.color = Color.parseColor("#512DA8")
            canvas.drawCircle(vCenter.x, vCenter.y, radius, paint)
            paint.color = Color.WHITE
            canvas.drawText(it.get("id").asString,vCenter.x-15*scale, vCenter.y+8*scale, paint)
        }

        tempPointList.forEach {
            sCenter.set(it.get("posx").asFloat, it.get("posy").asFloat)
            sourceToViewCoord(sCenter, vCenter)
            paint.strokeWidth = strokeWidth * 2.toFloat()
            paint.color = Color.parseColor("#FFC107")
            canvas.drawCircle(vCenter.x, vCenter.y, radius, paint)
            paint.color = Color.BLACK
            canvas.drawText(it.get("id").asString,vCenter.x-15*scale, vCenter.y+8*scale, paint)
        }
        tempPointList.clear()

    }

    private fun initialise() {
        val density = resources.displayMetrics.densityDpi.toFloat()
        strokeWidth = (density / 60f).toInt()
    }


    init {
        initialise()
    }
}