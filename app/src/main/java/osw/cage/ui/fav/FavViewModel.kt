package osw.cage.ui.fav

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import osw.cage.R
import osw.cage.db.DbWorker
import osw.cage.db.Favourite
import osw.cage.util.DlgUtil


open class FavItem(
    var title: String? = null,
    var item: Favourite,
    var model: FavViewModel
) : AbstractItem<FavItem.ViewHolder>() {

    override val type: Int
        get() = R.id.maplist

    override val layoutRes: Int
        get() = R.layout.list_item

    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v)
    }

    class ViewHolder(view: View) : FastAdapter.ViewHolder<FavItem>(view) {
        var name: TextView = view.findViewById(R.id.title)
        var description: TextView = view.findViewById(R.id.subtitle)
        var icon: ImageView = view.findViewById(R.id.icon)

        override fun bindView(item: FavItem, payloads: MutableList<Any>) {
            name.text = item.title
            description.text = item.item.doctype
            if (item.item.dm) {
                icon.setImageResource(R.drawable.ic_password)
            }
            itemView.setOnLongClickListener {
                DlgUtil.confirm(
                    itemView.context,
                    "Remove favourite",
                    "Please confirm removing ${item.item.title}",
                    {
                        DbWorker.postDbTask(itemView.context) { db ->
                            db.favouriteDao().delete(item.item)
                            item.model.update(itemView.context)
                            Toast.makeText(
                                itemView.context,
                                "${item.title} removed from favourites",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    },
                    {})
                false
            }
        }

        override fun unbindView(item: FavItem) {
            name.text = null
            description.text = null
        }
    }
}

class FavViewModel : ViewModel() {

    private val _list = MutableLiveData<List<FavItem>>().apply {
        value = listOf()
    }
    val list: LiveData<List<FavItem>> = _list

    fun update(context: Context) {
        DbWorker.postDbTask(context) { db ->
            var lst = db.favouriteDao().getAll()
            _list.postValue(lst.map { FavItem(it.title, it, this) }.toList())
        }
    }
}
