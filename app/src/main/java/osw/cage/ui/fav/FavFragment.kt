package osw.cage.ui.fav

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import kotlinx.android.synthetic.main.app_bar_main.*
import osw.cage.R
import osw.cage.db.*
import osw.cage.ui.card.CardGroupFragment
import osw.cage.ui.card.CardGroupItem
import osw.cage.ui.fav.FavItem
import osw.cage.ui.fav.FavViewModel
import osw.cage.ui.map.MapFragment
import osw.cage.ui.map.MapItem
import osw.cage.ui.pdf.PdfFragment
import osw.cage.ui.pdf.PdfItem
import osw.cage.ui.ref.JsonDataType
import osw.cage.ui.ref.RefFragment
import osw.cage.ui.ref.RefItem
import osw.cage.util.DlgUtil
import osw.cage.util.PrefUtil

class FavFragment : Fragment() {

    lateinit var recyclerView: RecyclerView
    private lateinit var favViewModel: FavViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_fav, container, false)
        recyclerView = root.findViewById(R.id.listView) as RecyclerView
        favViewModel =
            ViewModelProviders.of(this).get(FavViewModel::class.java)
        val itemAdapter = ItemAdapter<FavItem>()
        val fastAdapter = FastAdapter.with(itemAdapter)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = fastAdapter
        favViewModel.list.observe(this, Observer {
            itemAdapter.clear()
            itemAdapter.add(it)
        })
        fastAdapter.onClickListener = { view, adapter, item, position ->
            openFav(item)
            false
        }
        activity?.toolbar?.title = "Favourites"
        setHasOptionsMenu(true)
        favViewModel.update(context!!)
        return root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.fav_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_clear -> {
                DlgUtil.confirm(context!!, "Clear all favourites", "Please confirm", {
                    DbWorker.postDbTask(context) { db ->
                        db.favouriteDao().clear()
                        Toast.makeText(context, "Favourites cleared", Toast.LENGTH_SHORT).show()
                        favViewModel.update(context!!)
                    }
                }, {})
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun openFav(item: FavItem) {
        when (item.item.doctype) {
            FAV_MAP -> {
                if (item.item.dm) {
                    DlgUtil.inputPass(context!!, "DM Access", "Enter DM password", false) { pass, _ ->
                        if (pass == PrefUtil.getDmPassword()) {
                            openMap(item.item.data1!!, item.item.data2)
                        }
                    }
                } else {
                    openMap(item.item.data1!!, item.item.data2)
                }

            }
            FAV_PDF -> {
                if (item.item.dm) {
                    DlgUtil.inputPass(context!!, "DM Access", "Enter DM password", false) { pass, _ ->
                        if (pass == PrefUtil.getDmPassword()) {
                            openDoc(item.item.data1!!)
                        }
                    }
                } else {
                    openDoc(item.item.data1!!)
                }

            }
            FAV_REF -> {
                if (item.item.dm) {
                    DlgUtil.inputPass(
                        context!!,
                        "DM Access",
                        "Enter DM password",
                        false
                    ) { pass, _ ->
                        if (pass == PrefUtil.getDmPassword()) {
                            openRef(item.item.data1!!)
                        }
                    }
                } else {
                    openRef(item.item.data1!!)
                }
            }
            FAV_CARDGRP -> {
                DbWorker.postDbTask(context!!) { db ->
                    val item = db.cardgroupDao().findByName(item.title!!)
                    if (item != null) {
                       openGroup(item)
                    }
                }
            }
        }
    }

    fun openMap(filename: String, locations: String?) {
        val tr = fragmentManager!!.beginTransaction()
        tr.replace(R.id.nav_host_fragment, MapFragment(filename, locations))
        tr.addToBackStack(null)
        tr.commit()
    }

    fun openDoc(filename: String) {
        val tr = fragmentManager!!.beginTransaction()
        tr.replace(R.id.nav_host_fragment, PdfFragment(filename))
        tr.addToBackStack(null)
        tr.commit()
    }

    fun openRef(itemType: String) {
        val tp = JsonDataType.valueOf(itemType)
        val tr = fragmentManager!!.beginTransaction()
        tr.replace(R.id.nav_host_fragment, RefFragment(tp))
        tr.addToBackStack(null)
        tr.commit()
    }

    private fun openGroup(item: CardGroup) {
        val tr = fragmentManager!!.beginTransaction()
        tr.replace(R.id.nav_host_fragment, CardGroupFragment(item))
        tr.addToBackStack(null)
        tr.commit()
    }

}