package osw.cage.ui.pdf

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import kotlinx.android.synthetic.main.app_bar_main.*
import osw.cage.R
import osw.cage.db.DbWorker
import osw.cage.db.FAV_PDF
import osw.cage.db.Favourite
import osw.cage.util.DlgUtil
import osw.cage.util.PrefUtil

class PdfListFragment : Fragment() {

    lateinit var recyclerView: RecyclerView
    private lateinit var pdfViewModel: PdfViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_pdflist, container, false)
        recyclerView = root.findViewById(R.id.listView)
        pdfViewModel =
            ViewModelProviders.of(this).get(PdfViewModel::class.java)
        val itemAdapter = ItemAdapter<PdfItem>()
        val fastAdapter = FastAdapter.with(itemAdapter)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = fastAdapter
        pdfViewModel.list.observe(this, Observer {
            itemAdapter.add(it)
        })
        fastAdapter.onClickListener = { view, adapter, item, position ->
            if (item.dm) {
                DlgUtil.inputPass(context!!, "DM Access", "Enter DM password", false) { pass, _ ->
                    if (pass == PrefUtil.getDmPassword()) {
                        openDoc(item)
                    }
                }
            } else {
                openDoc(item)
            }
            false
        }
        fastAdapter.onLongClickListener = { _, _, item, _ ->
            DlgUtil.confirm(context!!, "Add favourite", "Please confirm adding ${item.name}", {
                DbWorker.postDbTask(context) { db ->
                    db.favouriteDao()
                        .insertAll(Favourite(null, item.name, FAV_PDF, item.filename, "", item.dm))
                    Toast.makeText(context, "${item.name} added to favourites", Toast.LENGTH_SHORT)
                        .show()
                }
            }, {})
            false
        }
        activity?.toolbar?.title = "PDF Documents"
        return root
    }

    fun openDoc(item: PdfItem) {
        val tr = fragmentManager!!.beginTransaction()
        tr.replace(R.id.nav_host_fragment, PdfFragment(item.filename!!))
        tr.addToBackStack(null)
        tr.commit()
    }

}