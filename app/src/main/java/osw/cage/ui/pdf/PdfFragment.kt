package osw.cage.ui.pdf

import android.graphics.pdf.PdfDocument
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.github.barteksc.pdfviewer.PDFView
import kotlinx.android.synthetic.main.app_bar_main.*
import osw.cage.MainActivity
import osw.cage.R
import osw.cage.db.DbWorker
import osw.cage.db.PdfBookmark
import osw.cage.util.DlgUtil
import osw.cage.util.PrefUtil
import java.io.File

class PdfFragment(val filename: String) : Fragment() {

    var pdfView: PDFView? = null
    var currentPage = 0
    var nightMode = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.pdf_fragment, container, false)
        pdfView = view.findViewById(R.id.pdfView)
        activity?.toolbar?.title = shortName()
        setHasOptionsMenu(true)
        nightMode = PrefUtil.isNightMode()
        refresh()
        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.pdf_menu, menu)
    }

    private fun flattenToc(
        lst: MutableList<com.shockwave.pdfium.PdfDocument.Bookmark>,
        root: com.shockwave.pdfium.PdfDocument.Bookmark
    ) {
        lst.add(root)
        if (root.hasChildren()) {
            root.children.forEach { flattenToc(lst, it) }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_pdflist -> {
                val tr = fragmentManager!!.beginTransaction()
                tr.replace(R.id.nav_host_fragment, PdfListFragment())
                tr.addToBackStack(null)
                tr.commit()
                return true
            }
            R.id.action_add_bmark -> {
                DlgUtil.input(context!!, "Bookmark name", "", "") {
                    DbWorker.postDbTask(context) { db ->
                        val bmark = PdfBookmark(null, filename, it, currentPage)
                        db.pdfBookmarkDao().insertAll(bmark)
                        Toast.makeText(context, "Bookmark $it saved", Toast.LENGTH_SHORT).show()
                    }

                }
            }
            R.id.action_list_bmark -> {
                DbWorker.postDbTask(context) { db ->
                    val bmlist = db.pdfBookmarkDao().getAll(filename)
                    if (bmlist.isNotEmpty()) {
                        DlgUtil.selectBs(
                            context!!,
                            "Select bookmark",
                            bmlist.map { it.name!! }.toList()
                        ) { pos, _ ->
                            val it = bmlist[pos]
                            currentPage = it.page!!
                            jump(currentPage)
                        }
                    }
                }
            }
            R.id.action_del_bmark -> {
                DbWorker.postDbTask(context) { db ->
                    val bmlist = db.pdfBookmarkDao().getAll(filename)
                    if (bmlist.isNotEmpty()) {
                        DlgUtil.selectBs(
                            context!!,
                            "Delete bookmark",
                            bmlist.map { it.name!! }.toList()
                        ) { pos, item ->
                            db.pdfBookmarkDao().delete(bmlist[pos])
                            Toast.makeText(context, "Bookmark $item deleted", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                }
            }
            R.id.action_clear_bmark -> {
                DlgUtil.confirm(context!!, "Clear bookmarks",
                    "Please confirm clearing all bookmarks", {
                        DbWorker.postDbTask(context) { db ->
                            db.pdfBookmarkDao().clear()
                            Toast.makeText(context, "Bookmarks cleared", Toast.LENGTH_SHORT).show()
                        }
                    }, {})
            }
            R.id.action_toc -> {
                val toc = pdfView!!.tableOfContents
                if (toc.isNotEmpty()) {
                    val lst =  mutableListOf<com.shockwave.pdfium.PdfDocument.Bookmark>()
                    toc.forEach {
                        flattenToc(lst, it)
                    }
                    DlgUtil.selectBs(
                        context!!,
                        "Table of Contents",
                        lst.map { it.title }) { pos, item ->
                        val tt = lst[pos]
                        currentPage = tt.pageIdx.toInt()
                        jump(currentPage)
                    }
                } else {
                    Toast.makeText(context, "Table of contents not found", Toast.LENGTH_SHORT)
                        .show()
                }
            }
            R.id.action_nightmode -> {
                nightMode = !nightMode
                refresh()
            }
            R.id.action_goto -> {
                DlgUtil.inputInt(context!!, "Goto page", "Page number", currentPage+1) {
                    if (it > 0 && it <= pdfView!!.pageCount) {
                        jump(it-1)
                    }
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun jump(page: Int) {
        activity?.runOnUiThread {
            pdfView!!.jumpTo(page)
        }
    }

    private fun shortName(): String {
        val parts = filename.split(File.separator)
        val result = parts.last()
            .replace(".pdf", "")
            .replace("%20", " ")
            .replace("_", " ")
        return result
    }

    private fun refresh() {

        pdfView!!
            .fromAsset(filename)
            .enableSwipe(true)
            .enableAntialiasing(true)
            .enableDoubletap(true)
            .swipeHorizontal(true)
            .pageSnap(true)
            .autoSpacing(true)
            .pageFling(true)
            .defaultPage(currentPage)
            .nightMode(nightMode)
            .enableAnnotationRendering(true)
            .onPageChange { x, y ->
                Toast.makeText(context, "${x + 1}/$y", Toast.LENGTH_SHORT).show()
                currentPage = x
            }
            .onLongPress {
                performLongClick(it!!)
            }
            .onLoad {
            }
            .load()
    }

    private fun performLongClick(event: MotionEvent) {
        val bar = (activity as MainActivity).supportActionBar!!
        if (bar.isShowing) {
            bar.hide()
        } else {
            bar.show()
        }
    }
}