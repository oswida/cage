package osw.cage.ui.pdf

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import osw.cage.R

open class PdfItem(
    var name: String? = null,
    var description: String? = null,
    var filename: String? = null,
    var dm: Boolean = false
) : AbstractItem<PdfItem.ViewHolder>() {

    override val type: Int
        get() = R.id.pdflist

    override val layoutRes: Int
        get() = R.layout.list_item

    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v)
    }

    class ViewHolder(view: View) : FastAdapter.ViewHolder<PdfItem>(view) {
        var name: TextView = view.findViewById(R.id.title)
        var description: TextView = view.findViewById(R.id.subtitle)
        var icon: ImageView = view.findViewById(R.id.icon)

        override fun bindView(item: PdfItem, payloads: MutableList<Any>) {
            name.text = item.name
            description.text = item.description
            if (item.dm) {
                icon.setImageResource(R.drawable.ic_password)
            }
        }

        override fun unbindView(item: PdfItem) {
            name.text = null
            description.text = null
        }
    }
}

class PdfViewModel : ViewModel() {
    private val _list = MutableLiveData<List<PdfItem>>().apply {
        value = listOf(
            PdfItem(
                "DnD Basic Rules", "Basic 5e rules from WoTC (2018)",
                "pdf/DnD_BasicRules.pdf"
            ),
            PdfItem(
                "DnD Conversions", "Conversions from older editions to 5e rules",
                "pdf/DnD_Conversions.pdf"
            ),
            PdfItem(
                "Planescape Player's Guide", "Player's guide by Chris Perkins",
                "pdf/Guide_to_Planescape.pdf"
            ),
            PdfItem(
                "Sigil Factions", "Short factions descriptions",
                "pdf/Sigil_Factions.pdf"
            ),
            PdfItem(
                "Player Reference", "All in one reference for players","pdf/Player_Reference.pdf"
            ),
            PdfItem(
              "Spellcasting Reference", "Quick ref for spellcasters", "pdf/Spellcast_Reference.pdf"
            ),
            PdfItem(
                "Faces of Sigil", "Who's Who in Sigil (Warning! DM material!)",
                "pdf/Faces_of_Sigil.pdf", true
            ),
            PdfItem(
                "Cosmographical Tables", "Places in Planes",
                "pdf/Cosmographical_Tables.pdf",true
            ),
            PdfItem(
                "Powers by Plane", "List of powers",
                "pdf/Powers_by_Plane.pdf", true
            )
        )
    }
    val list: LiveData<List<PdfItem>> = _list

}
