package osw.cage.ui.card

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.yuyakaido.android.cardstackview.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.html.*
import kotlinx.html.stream.appendHTML
import osw.cage.R
import osw.cage.db.*
import osw.cage.util.DlgUtil
import osw.cage.util.FileUtil

class CardGroupFragment(val group: CardGroup) : Fragment(), CardStackListener {

    companion object {
        const val IMPORT_FILE_CODE = 200
    }

    private lateinit var cardStackView: CardStackView
    private lateinit var layoutMgr: CardStackLayoutManager
    private lateinit var adapter: CardStackAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_cards, container, false)
        cardStackView = root.findViewById(R.id.cardstack)
        layoutMgr = CardStackLayoutManager(context, this)
        cardStackView.layoutManager = layoutMgr
        adapter = CardStackAdapter(group.id!!, activity!!)
        cardStackView.adapter = adapter
        layoutMgr.setStackFrom(StackFrom.Top)
        layoutMgr.setVisibleCount(3)
        setHasOptionsMenu(true)
        activity?.toolbar?.title = group.title
        refresh()
        return root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.card_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_import -> {
                val itn = Intent(Intent.ACTION_OPEN_DOCUMENT)
                itn.addCategory(Intent.CATEGORY_OPENABLE)
                itn.type = "*/*"
                startActivityForResult(itn, IMPORT_FILE_CODE)
            }
            R.id.action_rotate -> {
                rotateCurrentCard()
            }
            R.id.action_delete -> {
                deleteCurrentCard()
            }
            R.id.action_list -> {
                listCards()
            }
            R.id.action_add -> {
                DlgUtil.selectFile(context!!, FileUtil.appFile(context!!, ""), null) {
                    addCard(it.name)
                }
            }
            R.id.action_info -> cardInfo()
            R.id.action_rewind ->{
                cardStackView.rewind()
                adapter.notifyDataSetChanged()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, resultData: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                IMPORT_FILE_CODE -> {
                    if (resultData!!.data != null) {
                        val fname =
                            FileUtil.extractFilename(context!!.contentResolver, resultData!!.data!!)
                        FileUtil.copyFile(context, resultData!!.data!!, fname)
                        addCard(fname)
                    }
                }
            }
        }
    }

    fun refresh() {
        adapter.update(context!!) {
            activity?.runOnUiThread {
                adapter.notifyDataSetChanged()
            }
        }
    }


    private fun rotateCurrentCard() {
        adapter.rotateItem(layoutMgr.topPosition)
        adapter.notifyDataSetChanged()
    }

    private fun deleteCurrentCard() {
        if (adapter.itemCount > 0) {
            val crd = adapter.cards[layoutMgr.topPosition]
            DlgUtil.confirm(
                context!!,
                "Delete card",
                "Confirm deleting ${crd.card.title}",
                {
                    DbWorker.postDbTask(context) { db ->
                        db.cardDao().delete(crd.card)
                        if (db.cardDao().countForPath(crd.card.path!!) == 0L) {
                            FileUtil.deleteFile(context, crd.card.path!!)
                        }
                        refresh()
                    }
                },
                null
            )
        }
    }

    private fun addCard(fname: String) {
        DlgUtil.input(context!!, "Import card", "Card name", fname) { cardname ->
            DbWorker.postDbTask(context) { db ->
                var ct = "text"
                when {
                    fname.endsWith(".png") -> {
                        ct = CT_PNG
                    }
                    fname.endsWith(".jpg") -> {
                        ct = CT_JPG
                    }
                }
                db.cardDao().insertAll(Card(null, cardname, ct, fname, 0, group.id!!))
                Toast.makeText(context, "Card $cardname with $fname created", Toast.LENGTH_SHORT)
                    .show()
                refresh()
            }
        }
    }

    fun listCards() {
        DlgUtil.selectBs(
            context!!,
            "Select card",
            adapter.cards.map { it.card.title!! }) { pos, _ ->
            layoutMgr.scrollToPosition(pos)
        }
    }

    fun cardInfo() {
        val card = adapter.cards[layoutMgr.topPosition]
        val sb = StringBuilder()
        sb.appendHTML().html {
            body {
                ul {
                    li {
                        b { +"Value: " }
                        +card.card.position.toString()
                    }
                    li {
                        b { +"Deck position: " }
                        +layoutMgr.topPosition.toString()
                    }
                    li {
                        b { +"Total number of cards: " }
                        +adapter.itemCount.toString()
                    }
                    li {
                        b { +"Path: " }
                        +card.card.path!!
                    }
                }
            }
        }
        DlgUtil.webinfo(context!!, card.card.title!!, sb.toString())
    }

    override fun onCardDisappeared(view: View?, position: Int) {
    }

    override fun onCardDragging(direction: Direction?, ratio: Float) {
    }

    override fun onCardSwiped(direction: Direction?) {
        if (layoutMgr.topPosition >= adapter.itemCount) {
            cardStackView.rewind()
            adapter.notifyDataSetChanged()
        }
    }

    override fun onCardCanceled() {
    }

    override fun onCardAppeared(view: View?, position: Int) {
        val card = adapter.cards[position]
        Toast.makeText(
            context!!,
            "${card.card.title}   ${layoutMgr.topPosition + 1}/${adapter.itemCount}",
            Toast.LENGTH_SHORT
        ).show()
    }

    override fun onCardRewound() {
    }

}