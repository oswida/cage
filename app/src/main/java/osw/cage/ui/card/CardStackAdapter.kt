package osw.cage.ui.card

import android.app.Activity
import android.content.Context
import android.opengl.Visibility
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.davemorrissey.labs.subscaleview.ImageSource
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView.ORIENTATION_0
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView.ORIENTATION_90
import osw.cage.R
import osw.cage.db.CT_JPG
import osw.cage.db.CT_PNG
import osw.cage.db.Card
import osw.cage.db.DbWorker
import osw.cage.ui.map.PinView
import osw.cage.util.DlgUtil
import osw.cage.util.FileUtil

class CardItem(
    val card: Card,
    var rotated: Boolean = false,
    var note: String = "",
    var showNote: Boolean = false
)

class CardStackAdapter(
    val grpId: Long,
    val activity: Activity,
    var cards: List<CardItem> = emptyList()
) : RecyclerView.Adapter<CardStackAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.item_card, parent, false))
    }

    private fun setCardValue(holder: ViewHolder, position: Int) {
        DlgUtil.inputInt(
            holder.itemView.context,
            "Set card value",
            "",
            cards[position].card.position
        ) { pos ->
            DbWorker.postDbTask(holder.itemView.context) { db ->
                cards[position].card.position = pos
                db.cardDao().save(cards[position].card)
                Toast.makeText(holder.itemView.context, "Value saved", Toast.LENGTH_SHORT)
                    .show()
                update(holder.itemView.context) {
                    activity.runOnUiThread {
                        notifyDataSetChanged()
                    }
                }
            }
        }
    }

    private fun setNote(holder: ViewHolder, position: Int) {
        DlgUtil.input(
            holder.itemView.context,
            "Set card note",
            "",
            cards[position].note
        ) { txt ->
            if (txt != null) {
                cards[position].note = txt
            } else {
                cards[position].note = ""
            }
            if (cards[position].note.isNotBlank()) {
                holder.cardText.text = cards[position].note
            } else {
                holder.cardText.text = ""
            }
        }
    }

    private fun toggleNote(holder: ViewHolder, position: Int) {
        cards[position].showNote = !cards[position].showNote
        if (cards[position].showNote) {
            holder.cardText.visibility = View.VISIBLE
        } else {
            holder.cardText.visibility = View.GONE
        }
    }

    private fun toggleAllNotes(holder: ViewHolder) {
        for(position in cards.indices) {
            cards[position].showNote = !cards[position].showNote
        }
        activity.runOnUiThread {
            notifyDataSetChanged()
        }
    }

    private fun resetValues(holder: ViewHolder) {
        DlgUtil.inputInt(
            holder.itemView.context,
            "Reset all values",
            "New value", null
        ) { value ->
            DbWorker.postDbTask(holder.itemView.context) { db ->
                val lst = db.cardDao().getAllForGroup(grpId)
                lst.forEach {
                    it.position = value
                    db.cardDao().save(it)
                }
                Toast.makeText(holder.itemView.context, "Values reset", Toast.LENGTH_SHORT)
                    .show()
                update(holder.itemView.context) {
                    activity.runOnUiThread {
                        notifyDataSetChanged()
                    }
                }
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val card = cards[position]
        when (card.card.type) {
            CT_JPG, CT_PNG -> {
                if (card.rotated) {
                    holder.cardImage.orientation = ORIENTATION_90
                } else {
                    holder.cardImage.orientation = ORIENTATION_0
                }
                holder.cardImage.setImage(
                    ImageSource.uri(
                        FileUtil.appFile(
                            holder.itemView.context,
                            card.card.path!!
                        ).absolutePath
                    )
                )
                if (card.note.isNotBlank()) {
                    holder.cardText.text = card.note
                } else {
                    holder.cardText.text = ""
                }
                if (card.showNote) {
                    holder.cardText.visibility = View.VISIBLE
                } else {
                    holder.cardText.visibility = View.GONE
                }
            }
            else -> holder.cardText.text = card.card.title!!
        }
        holder.cardImage.setOnLongClickListener {
            DlgUtil.selectBs(
                holder.itemView.context,
                "",
                listOf("Set card value", "Reset values", "Set note text", "Toggle note", "Toggle all notes")
            ) { pos, _ ->
                when (pos) {
                    0 -> setCardValue(holder, position)
                    1 -> resetValues(holder)
                    2 -> setNote(holder, position)
                    3 -> toggleNote(holder, position)
                    4 -> toggleAllNotes(holder)
                }
            }
            true
        }
//        holder.cardText.setOnLongClickListener {
//            setCardValue(holder, position)
//            true
//        }
    }

    fun rotateItem(position: Int) {
        if (cards[position].card.type == CT_JPG || cards[position].card.type == CT_PNG) {
            cards[position].rotated = !cards[position].rotated
        }
    }

    override fun getItemCount(): Int {
        return cards.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val cardText: TextView = view.findViewById(R.id.cardText)
        val cardImage: PinView = view.findViewById(R.id.cardImage)
    }

    fun update(context: Context, func: (() -> Unit)? = null) {
        DbWorker.postDbTask(context) { db ->
            cards =
                db.cardDao().getAllForGroup(grpId).sortedBy { it.position }.sortedBy { it.title }
                    .map { CardItem(it) }
            if (func != null) {
                func()
            }
        }
    }
}