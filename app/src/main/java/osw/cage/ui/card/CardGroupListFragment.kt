package osw.cage.ui.card

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import kotlinx.android.synthetic.main.app_bar_main.*
import osw.cage.R
import osw.cage.db.CardGroup
import osw.cage.db.DbWorker
import osw.cage.db.FAV_CARDGRP
import osw.cage.db.Favourite
import osw.cage.util.DlgUtil

class CardGroupListFragment : Fragment() {
    lateinit var recyclerView: RecyclerView
    private lateinit var cgViewModel: CardGroupViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_cglist, container, false)
        recyclerView = root.findViewById(R.id.listView)
        cgViewModel =
            ViewModelProviders.of(this).get(CardGroupViewModel::class.java)
        val itemAdapter = ItemAdapter<CardGroupItem>()
        val fastAdapter = FastAdapter.with(itemAdapter)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = fastAdapter
        cgViewModel.list.observe(this, Observer {
            itemAdapter.clear()
            itemAdapter.add(it)
        })
        val addBtn = root.findViewById<FloatingActionButton>(R.id.addBtn)
        addBtn.setOnClickListener {
            DlgUtil.input(context!!, "Add card group", "Group name", "") {
                if (it != null && it.isNotEmpty()) {
                    DbWorker.postDbTask(context) { db ->
                        db.cardgroupDao().insertAll(CardGroup(null, it, ""))
                        cgViewModel.update(context!!)
                    }
                }
            }
        }
        fastAdapter.onClickListener = { _, _, item, _ ->
            openGroup(item)
            false
        }
        fastAdapter.onLongClickListener = { _, _, item, _ ->
            DlgUtil.selectBs(
                context!!,
                "",
                listOf("Rename", "Delete", "Add to favourites")
            ) { pos, _ ->
                when (pos) {
                    0 -> renameItem(item.item!!)
                    1 -> deleteItem(item.item!!)
                    2 -> favItem(item.item!!)
                }
            }
            false
        }
        activity?.toolbar?.title = "Card Groups"
        cgViewModel.update(context!!)
        return root
    }

    fun deleteItem(item: CardGroup) {
        DlgUtil.confirm(
            context!!,
            "Delete card group",
            "Please confirm deleting ${item!!.title} and all its cards!",
            {
                DbWorker.postDbTask(context!!) { db ->
                    db.cardgroupDao().delete(item!!)
                    cgViewModel.update(context!!)
                    Toast.makeText(
                        context!!,
                        "${item!!.title} deleted",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
            },
            null
        )
    }

    fun renameItem(item: CardGroup) {
        DlgUtil.input(context!!, "Rename card group", "Group name", item.title!!) {
            if (it != null && it.isNotEmpty()) {
                DbWorker.postDbTask(context) { db ->
                    item.title = it
                    db.cardgroupDao().save(item)
                    cgViewModel.update(context!!)
                }
            }
        }
    }

    fun favItem(item: CardGroup) {
        DlgUtil.confirm(
            context!!,
            "Add favourite",
            "Please confirm adding ${item.title!!}",
            {
                DbWorker.postDbTask(context) { db ->
                    db.favouriteDao()
                        .insertAll(
                            Favourite(
                                null,
                                item.title!!,
                                FAV_CARDGRP,
                                item.title!!,
                                "",
                                false
                            )
                        )
                    Toast.makeText(
                        context,
                        "${item.title!!} added to favourites",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
            },
            {})
    }

    private fun openGroup(item: CardGroupItem) {
        val tr = fragmentManager!!.beginTransaction()
        tr.replace(R.id.nav_host_fragment, CardGroupFragment(item.item!!))
        tr.addToBackStack(null)
        tr.commit()
    }
}