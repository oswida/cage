package osw.cage.ui.card

import android.content.Context
import android.view.View
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import osw.cage.R
import osw.cage.db.CardGroup
import osw.cage.db.DbWorker


open class CardGroupItem(
    var item: CardGroup? = null
) : AbstractItem<CardGroupItem.ViewHolder>() {

    override val type: Int
        get() = R.id.cglist

    override val layoutRes: Int
        get() = R.layout.list_item

    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v)
    }

    class ViewHolder(view: View) : FastAdapter.ViewHolder<CardGroupItem>(view) {
        var name: TextView = view.findViewById(R.id.title)
        var description: TextView = view.findViewById(R.id.subtitle)

        override fun bindView(item: CardGroupItem, payloads: MutableList<Any>) {
            name.text = item.item!!.title
            description.text = item.item!!.description
        }

        override fun unbindView(item: CardGroupItem) {
            name.text = null
            description.text = null
        }
    }
}

class CardGroupViewModel : ViewModel() {

    private val _list = MutableLiveData<List<CardGroupItem>>().apply {
        value = listOf()
    }
    val list: LiveData<List<CardGroupItem>> = _list

    fun update(context: Context) {
        DbWorker.postDbTask(context) { db ->
           _list.postValue(db.cardgroupDao().getAll().map { CardGroupItem(it) })
        }
    }
}