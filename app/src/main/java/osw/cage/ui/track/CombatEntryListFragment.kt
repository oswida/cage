package osw.cage.ui.track

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import kotlinx.android.synthetic.main.app_bar_main.*
import osw.cage.R
import osw.cage.db.CombatEntry
import osw.cage.db.DbWorker
import osw.cage.ui.ref.JsonData
import osw.cage.ui.ref.RefFragment
import osw.cage.ui.ref.RefParser
import osw.cage.util.DlgUtil
import osw.cage.util.FileUtil
import java.lang.reflect.Type

class CombatEntryListFragment : Fragment() {

    lateinit var recyclerView: RecyclerView
    private lateinit var ceViewModel: CombatEntryViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_celist, container, false)
        recyclerView = root.findViewById(R.id.listView) as RecyclerView
        ceViewModel =
            ViewModelProviders.of(this).get(CombatEntryViewModel::class.java)
        val itemAdapter = ItemAdapter<CombateItem>()
        val fastAdapter = FastAdapter.with(itemAdapter)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = fastAdapter
        ceViewModel.list.observe(this, Observer {
            itemAdapter.clear()
            itemAdapter.add(it)
        })
        val addBtn = root.findViewById<FloatingActionButton>(R.id.addBtn)
        addBtn.setOnClickListener {
            DlgUtil.selectBs(
                context!!,
                "Add entry",
                listOf("Monster from reference", "Manual")
            ) { pos, _ ->
                when (pos) {
                    0 -> {
                        addMonster()
                    }
                    1 -> {
                        addManual()
                    }
                }
            }
        }
        fastAdapter.onClickListener = { _, _, item, _ ->
            val tr = fragmentManager!!.beginTransaction()
            tr.replace(R.id.nav_host_fragment, CombatEntryEditor(item.item))
            tr.addToBackStack(null)
            tr.commit()
            false
        }
        fastAdapter.onLongClickListener = { _, _, item, _ ->
            DlgUtil.confirm(context!!,"Delete entry", "Confirm delete of ${item.item.title}", {
                DbWorker.postDbTask(context!!) { db ->
                    db.combateDao().delete(item.item)
                    Toast.makeText(context!!, "${item.item.title} deleted", Toast.LENGTH_SHORT).show()
                    ceViewModel.update(context!!)
                }
            },{})
            false
        }
        activity?.toolbar?.title = "Combat Entries"
        ceViewModel.update(context!!)
        return root
    }

    fun addMonster() {
        val retv = mutableListOf<JsonData>()
        val data = FileUtil.assetFile(context!!, "json/bestiary.json")
        val listType: Type = object : TypeToken<List<JsonObject?>?>() {}.type
        val lst: List<JsonObject> = Gson().fromJson(data, listType)
        try {
            lst.forEach {
                retv.add(RefParser.parseBeast(it))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        DlgUtil.selectWithSearch(
            activity!!,
            "Select entry",
            retv.map { it.title },
            0
        ) { pos, text ->
            val item = lst[pos]
            DbWorker.postDbTask(context!!) { db ->
                db.combateDao()
                    .insertAll(
                        CombatEntry(
                            null,
                            item.get("name").asString,
                            "0",
                            RefParser.extractBeastAc(item),
                            RefParser.extractBeastHp(item),
                            item.get("passive").asString,
                            "",
                            "", "", "",
                            true,
                            json = item.toString()
                        )
                    )
                ceViewModel.update(context!!)
            }
        }
    }

    fun addManual() {
        val entry = CombatEntry(null, "Manual", "", "", "", "", "", "", "", "", false, "")
        val tr = fragmentManager!!.beginTransaction()
        tr.replace(R.id.nav_host_fragment, CombatEntryEditor(entry))
        tr.addToBackStack(null)
        tr.commit()
    }


}