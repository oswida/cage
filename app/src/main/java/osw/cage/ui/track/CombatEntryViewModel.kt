package osw.cage.ui.track

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import osw.cage.R
import osw.cage.db.CombatEntry
import osw.cage.db.DbWorker


open class CombateItem(
    var title: String? = null,
    var item: CombatEntry,
    var model: CombatEntryViewModel
) : AbstractItem<CombateItem.ViewHolder>() {

    override val type: Int
        get() = R.id.maplist

    override val layoutRes: Int
        get() = R.layout.list_item

    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v)
    }

    class ViewHolder(view: View) : FastAdapter.ViewHolder<CombateItem>(view) {
        var name: TextView = view.findViewById(R.id.title)
        var description: TextView = view.findViewById(R.id.subtitle)
        var icon: ImageView = view.findViewById(R.id.icon)

        override fun bindView(item: CombateItem, payloads: MutableList<Any>) {
            name.text = item.title
            description.text = "${ if (item.item.monster) {"Bestiary"} else { "Manual"}} / AC: ${item.item.ac} / HP: ${item.item.hp}"
//            if (item.item.dm) {
//                icon.setImageResource(R.drawable.ic_password)
//            }
        }

        override fun unbindView(item: CombateItem) {
            name.text = null
            description.text = null
        }
    }
}

class CombatEntryViewModel : ViewModel() {

    private val _list = MutableLiveData<List<CombateItem>>().apply {
        value = listOf()
    }
    val list: LiveData<List<CombateItem>> = _list

    fun update(context: Context) {
        DbWorker.postDbTask(context) { db ->
            var lst = db.combateDao().getAll()
            _list.postValue(lst.map { CombateItem(it.title, it, this) }.toList())
        }
    }
}
