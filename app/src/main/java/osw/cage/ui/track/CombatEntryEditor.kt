package osw.cage.ui.track

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.thejuki.kformmaster.helper.*
import com.thejuki.kformmaster.model.BaseFormElement
import kotlinx.android.synthetic.main.app_bar_main.*
import osw.cage.R
import osw.cage.db.CombatEntry
import osw.cage.db.DbWorker
import osw.cage.ui.ref.RefFragment
import osw.cage.ui.tools.ToolsFragment
import osw.cage.util.DlgUtil
import osw.cage.util.MiscUtil


class CombatEntryEditor(val item: CombatEntry) : Fragment() {

    private val NAME_TAG = 200001
    private val AC_TAG = 200002
    private val HP_TAG = 200003
    private val PP_TAG = 200004
    private val SSDC_TAG = 200005
    private val ADV_TAG = 200006
    private val DISADV_TAG = 200007
    private val NOTES_TAG = 200008
    private val HEADER_TAG = 200009

    private lateinit var recyclerView: RecyclerView
    private var formBuilder: FormBuildHelper? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_ceedit, container, false)
        recyclerView = root.findViewById(R.id.listView)
        initForm()
        activity?.toolbar?.title = item.title
        setHasOptionsMenu(true)
        return root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.ceedit_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_back -> {
                backToList()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun backToList() {
        MiscUtil.hideKeyboard(context!!, view!!)
        val tr = fragmentManager!!.beginTransaction()
        tr.replace(R.id.nav_host_fragment, CombatEntryListFragment())
        tr.addToBackStack(null)
        tr.commit()
    }

    fun initForm() {
        formBuilder = form(context!!, recyclerView!!) {
            text {
                tag = NAME_TAG
                title = "Name"
                required = true
                centerText = true
                value = ""
            }
            text {
                tag = AC_TAG
                title = "Armor Class"
                required = true
                centerText = true
                value = ""
            }
            text {
                tag = HP_TAG
                title = "HP"
                required = true
                centerText = true
                value = ""
            }
            text {
                tag = PP_TAG
                title = "Passive Perception"
                required = true
                centerText = true
                value = ""
            }
            text {
                tag = SSDC_TAG
                title = "Spell Save DC"
                required = true
                centerText = true
                value = ""
            }
            header {
                tag = HEADER_TAG
                title = "Advantages"
            }
            textArea {
                tag = ADV_TAG
                displayTitle = false
                rightToLeft = false
                value = ""
            }
            header {
                tag = HEADER_TAG
                title = "DisAdvantages"
            }
            textArea {
                tag = DISADV_TAG
                displayTitle = false
                rightToLeft = false
                value = ""
            }
            header {
                tag = HEADER_TAG
                title = "Notes"
            }
            textArea {
                tag = NOTES_TAG
                displayTitle = false
                rightToLeft = false
                value = ""
            }
            button {
                value = "SAVE"
                valueObservers.add { _, _ ->
                    save()
                }
            }
        }
        formBuilder!!.attachRecyclerView(context!!, recyclerView)
        // update data
        updateElement(NAME_TAG, item.title!!)
        updateElement(AC_TAG, item.ac!!)
        updateElement(HP_TAG, item.hp!!)
        updateElement(PP_TAG, item.passivePerception!!)
        updateElement(SSDC_TAG, item.spellSaveDC!!)
        updateElement(ADV_TAG, item.advantages!!)
        updateElement(DISADV_TAG, item.disadvantages!!)
        updateElement(NOTES_TAG, item.notes!!)
    }

    fun updateElement(tag: Int, value: String) {
        val element = formBuilder!!.getFormElement<BaseFormElement<String>>(tag)
        element.value = value
    }

    fun getElementValue(tag: Int): String {
        val element = formBuilder!!.getFormElement<BaseFormElement<String>>(tag)
        return element.value.toString()
    }

    fun save() {
        DbWorker.postDbTask(context!!) { db ->
            item.title = getElementValue(NAME_TAG)
            item.ac = getElementValue(AC_TAG)
            item.hp = getElementValue(HP_TAG)
            item.passivePerception = getElementValue(PP_TAG)
            item.spellSaveDC = getElementValue(SSDC_TAG)
            item.advantages = getElementValue(ADV_TAG)
            item.disadvantages = getElementValue(DISADV_TAG)
            item.notes = getElementValue(NOTES_TAG)
            if (item.id != null) {
                db.combateDao().save(item)
            } else {
                db.combateDao().insertAll(item)
            }
            Toast.makeText(context!!, "${item.title} saved", Toast.LENGTH_SHORT).show()
            backToList()
        }
    }

}