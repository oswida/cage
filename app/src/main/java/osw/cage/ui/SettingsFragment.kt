package osw.cage.ui

import android.os.Bundle
import android.text.InputType
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import androidx.preference.EditTextPreference
import androidx.preference.PreferenceFragmentCompat
import osw.cage.R
import osw.cage.util.DlgUtil
import osw.cage.util.PrefUtil

class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
        setHasOptionsMenu(true)
//        val editTextPreference: EditTextPreference? = findPreference("pk_dm_password")
//
//        editTextPreference?.setOnBindEditTextListener {editText ->
//            editText.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
//        }
//
//        editTextPreference?.setOnPreferenceClickListener {
//            DlgUtil.inputPass(context!!,"DM Password", "Old password",false ) {
//                text, _ ->
//                if (text == PrefUtil.getDmPassword()) {
//                    false
//                }
//            }
//            true
//        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.settings_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_dm_pass -> {
                DlgUtil.inputPass(
                    context!!,
                    "Change DM password",
                    "Current password",
                    false
                ) { text, _ ->
                    if (text == PrefUtil.getDmPassword()) {
                        DlgUtil.inputPass(
                            context!!,
                            "Change DM password",
                            "New password",
                            false
                        ) { txt, _ ->
                            if (txt!!.isNotEmpty()) {
                                PrefUtil.setDmPassword(txt)
                                Toast.makeText(context!!, "DM password changed", Toast.LENGTH_SHORT)
                                    .show()
                            }
                        }
                    }
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }


}