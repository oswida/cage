package osw.cage.ui.ref

import android.os.Bundle
import android.view.*
import android.webkit.WebView
import android.widget.ImageView
import androidx.appcompat.widget.SearchView
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.app_bar_main.*
import osw.cage.R
import osw.cage.util.DlgUtil
import osw.cage.util.FileUtil
import java.lang.reflect.Type


class RefFragment(val viewType: JsonDataType) : Fragment() {

    lateinit var pager: ViewPager
    var items: List<JsonData>? = null
    var filter = ""


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_ref, container, false)
        pager = root.findViewById(R.id.pager)
        items = loadData()
        pager.adapter = RefAdapter(items!!, context!!)
        setHasOptionsMenu(true)
        when (viewType) {
            JsonDataType.RACE -> activity?.toolbar?.title = "Ref: Races"
            JsonDataType.BACKGROUND -> activity?.toolbar?.title = "Ref: Bkgs"
            JsonDataType.CLASS -> activity?.toolbar?.title = "Ref: Classes"
            JsonDataType.RULES -> activity?.toolbar?.title = "Ref: Rules"
            JsonDataType.CONDITION -> activity?.toolbar?.title = "Ref: Conditions"
            JsonDataType.FEAT -> activity?.toolbar?.title = "Ref: Features"
            JsonDataType.BEAST -> activity?.toolbar?.title = "Ref: Bestiary"
            JsonDataType.ITEM -> activity?.toolbar?.title = "Ref: Items"
            JsonDataType.PSIONICS -> activity?.toolbar?.title = "Ref: Psionics"
            JsonDataType.SPELL -> activity?.toolbar?.title = "Ref: Spells"
            JsonDataType.PLANESCAPE_BEAST -> activity?.toolbar?.title = "Ref: PS Bestiary"
        }
        return root
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.ref_menu, menu)
        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView.queryHint = "Filter repos"
        val v = searchView.findViewById(androidx.appcompat.R.id.search_button) as ImageView
        v.setImageResource(R.drawable.ic_filter_white)
        if (!filter.isEmpty()) {
            searchView.setQuery(filter, false)
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                var tt = newText
                if (tt === null) {
                    tt = ""
                }
                filter = tt
                if (tt.isNotBlank()) {
                    pager.adapter =
                        RefAdapter(items!!.filter { it.title.contains(filter, true) }, context!!)
                } else {
                    pager.adapter = RefAdapter(items!!, context!!)
                }
                return true
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_reflist -> {
                val tr = fragmentManager!!.beginTransaction()
                tr.replace(R.id.nav_host_fragment, RefListFragment())
                tr.addToBackStack(null)
                tr.commit()
                return true
            }
            R.id.action_toc -> {
                DlgUtil.selectWithSearch(
                    activity!!,
                    "Select item",
                    items!!.map { it.title }.toList(),
                    pager.currentItem
                ) { pos, _ ->
                    pager.setCurrentItem(pos, true)
                }
                return true
            }
            R.id.action_find -> {
                DlgUtil.input(context!!, "Find text", "Searched phrase", "") {
                    if (it!!.isNotBlank()) {
                        val wv = pager.get(pager.currentItem) as WebView
                        wv.findAllAsync(it)
                    }
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun loadData(): List<JsonData> {
        var filename = ""
        when (viewType) {
            JsonDataType.RACE -> filename = "json/races.json"
            JsonDataType.BACKGROUND -> filename = "json/backgrounds.json"
            JsonDataType.RULES -> filename = "json/reference.json"
            JsonDataType.CLASS -> filename = "json/classes.json"
            JsonDataType.CONDITION -> filename = "json/conditions.json"
            JsonDataType.FEAT -> filename = "json/feats.json"
            JsonDataType.BEAST-> filename = "json/bestiary.json"
            JsonDataType.ITEM-> filename = "json/items.json"
            JsonDataType.PSIONICS -> filename = "json/psionics.json"
            JsonDataType.SPELL -> filename = "json/spells.json"
            JsonDataType.PLANESCAPE_BEAST -> filename = "json/planescape_bestiary.json"
        }
        val retv = mutableListOf<JsonData>()
        val data = FileUtil.assetFile(context!!, filename)
        val listType: Type = object : TypeToken<List<JsonObject?>?>() {}.type
        try {
            val lst: List<JsonObject> = Gson().fromJson(data, listType)
            lst.forEach {
                when (viewType) {
                    JsonDataType.RACE -> retv.add(RefParser.parseRace(it))
                    JsonDataType.BACKGROUND -> retv.add(RefParser.parseBackground(it))
                    JsonDataType.CONDITION -> retv.add(RefParser.parseCondition(it))
                    JsonDataType.FEAT -> retv.add(RefParser.parseFeat(it))
                    JsonDataType.BEAST -> retv.add(RefParser.parseBeast(it))
                    JsonDataType.CLASS ->  retv.add(RefParser.parseClass(it))
                    JsonDataType.RULES -> retv.addAll(RefParser.parseRules(it))
                    JsonDataType.ITEM -> retv.add(RefParser.parseItem(it))
                    JsonDataType.PSIONICS -> retv.add(RefParser.parsePsionics(it))
                    JsonDataType.SPELL -> retv.add(RefParser.parseSpell(it))
                    JsonDataType.PLANESCAPE_BEAST -> retv.add(RefParser.parseBeast(it))
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return retv.toList().sortedBy { it.title }
    }


}