package osw.cage.ui.ref

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import kotlinx.html.*
import kotlinx.html.stream.appendHTML
import osw.cage.util.FileUtil
import osw.cage.util.PrefUtil
import java.lang.reflect.Type
import java.util.*

object RefParser {

    fun extractBeastAc(item: JsonObject): String {
        val ac = item.getAsJsonArray("ac")[0]
        if (ac is JsonObject) {
            if (ac.asJsonObject.has("ac")) {
                return ac.get("ac").asString
            }
        } else {
            return ac.asString
        }
        return ""
    }

    fun extractBeastHp(item: JsonObject): String {
        if (item.has("hp")) {
            return item.get("hp").asJsonObject.get("average").asString
        }
        return ""
    }

    private fun HEAD.refstyle(nightMode: Boolean) {
        style {
            unsafe {
                if (nightMode) {
                    raw(
                        """
                        body {
                           background-color: #222222 !important;
                           color: white !important;
                        }
                        table {
                            text-align: center;
                            width: 100%;
                            border: 1px solid #cccccc;
                            border-collapse: collapse;
                        }
                        td {
                            border: 1px solid #cccccc;
                            padding: 3px;
                        }
                        th {
                            background-color: #cccccc;
                            color: black;
                            padding: 3px;
                        }
                        .center {
                            text-align: center;
                        }
                        .inset {
                             background-color: #333333;
                             padding: 10px;
                        }
                        a {
                            color: #FFC107 !important;
                        }
                    """.trimIndent()
                    )
                } else {
                    raw(
                        """
                        table {
                            text-align: center;
                            width: 100%;
                            border: 1px solid #cccccc;
                            border-collapse: collapse;
                        }
                        td {
                            border: 1px solid #cccccc;
                            padding: 3px;
                        }
                        th {
                            background-color: #cccccc;
                            padding: 3px;
                        }
                        .center {
                            text-align: center;
                        }
                        .inset {
                          background-color: #eeeeee;
                          padding: 10px;
                        }
                        a {
                           color: #512DA8 !important;
                        }
                    """.trimIndent()
                    )
                }
            }
        }
    }

    private fun filterAts(text: String): String {
        val r1 =
            "\\{@(dice|filter|damage|book|5etools|skill) ([^\\}|]+)[^\\}]*\\}".toRegex()
        var retv = r1.replace(text) { "<u>${it.groupValues[2]}</u>" }
        val r2 = "\\{@(b) ([^\\}|]+)[^\\}]*\\}".toRegex()
        retv = r2.replace(retv) { "<b>${it.groupValues[2]}</b>" }
        val r3 = "\\{@(i|action) ([^\\}|]+)[^\\}]*\\}".toRegex()
        retv = r3.replace(retv) { "<i>${it.groupValues[2]}</i>" }
        val r5 = "\\{@(atk) ([^\\}|]+)[^\\}]*\\}".toRegex()
        retv = r5.replace(retv) { "ATK: ${it.groupValues[2]}" }
        val r4 = "\\{@hit ([^\\}]+)\\}".toRegex()
        retv = r4.replace(retv) { "+${it.groupValues[1]}" }
        val r6 = "\\{@h([^\\}]*)\\}".toRegex()
        retv = r6.replace(retv) { "<i>Hit: </i>" }
        val r7 = "\\{@dc ([^\\}]+)\\}".toRegex()
        retv = r7.replace(retv) { "<b>DC ${it.groupValues[1]}</b>" }
        val r8 =
            "\\{@(spell|item|condition|background|class|creature) ([^\\}|]+)[^\\}]*\\}".toRegex()
        retv =
            r8.replace(retv) { "<a href=\"${it.groupValues[1]}://${it.groupValues[2]}\">${it.groupValues[2]}</a>" }
        return retv
    }

    private fun makeDice(it: JsonElement): String {
        if (it is JsonObject) {
            var n = "0"
            var f = "0"
            if (it.has("number")) {
                n = it.get("number").asString
            }
            if (it.has("faces")) {
                f = it.get("faces").asString
            }
            return "${n}D$f"
        } else {
            return it.toString()
        }
    }

    private fun makeAbilityTd(it: JsonElement, name: String): String {
        if (it.asJsonObject.has(name)) {
            return it.asJsonObject.get(name).asString
        } else {
            return "0"
        }
    }

    private fun makeObjectStr(it: JsonElement): String {
        when (it) {
            is JsonObject -> {
                val lst = mutableListOf<String>()
                val obj = it.asJsonObject
                obj.keySet().forEach {
                    val vv = obj.get(it)
                    if (vv is JsonObject || vv is JsonArray) {
                        lst.add("$it (${makeObjectStr(vv)})")
                    } else {
                        val str = vv.asString
                        if (str == "true") {
                            lst.add("$it")
                        } else {
                            lst.add("$it ($str)")
                        }
                    }
                }
                return lst.joinToString(", ")
            }
            is JsonArray -> {
                val arr = it.asJsonArray
                val lst = mutableListOf<String>()
                arr.forEach { item ->
                    lst.add(makeObjectStr(item))
                }
                return lst.joinToString("; ")
            }
            else -> {
                var txt = ""
                try {
                    txt = it.asString
                } catch (ex: Exception) {
                    txt = it.toString()
                }
                return txt
            }
        }
    }

    private fun makeListString(it: JsonElement): String {
        if (it is JsonArray) {
            val arr = it.asJsonArray
            val lst = mutableListOf<String>()
            arr.forEach {
                if (it is JsonObject) {
                    lst.add(it.toString())
                } else {
                    lst.add(it.asString)
                }
            }
            return lst.joinToString(", ")
        }
        return ""
    }

    private fun makeProficiencies(it: JsonElement): String {
        if (it is JsonObject && it.asJsonObject.has("choose")) {
            val obj = it.asJsonObject.get("choose").asJsonObject
            val lst = mutableListOf<String>()
            if (obj.has("from")) {
                obj.get("from").asJsonArray.forEach { ff ->
                    if (ff is JsonObject) {
                        lst.add(ff.toString())
                    } else {
                        lst.add(ff.asString)
                    }
                }
            }
            var cnt = 1
            if (obj.has("count")) {
                cnt = obj.get("count").asInt
            } else if (obj.has("amount")) {
                cnt = obj.get("amount").asInt
            }
            return "Choose $cnt from ${lst.joinToString(", ")}"
        } else if (it is JsonObject) {
            return makeObjectStr(it)
        }
        return ""
    }

    fun P.entries(it: JsonElement, showName: Boolean = true) {
        if (it is JsonObject) {
            if (it.asJsonObject.has("name") && showName) {
                br
                b {
                    +it.asJsonObject.get("name").asString
                    +": "
                }
            }
            var etype = "entries"
            if (it.asJsonObject.has("type")) {
                etype = it.get("type").asString
            }
            when (etype) {
                "entries", "inline", "quote" -> {
                    val arr = it.get("entries").asJsonArray
                    arr.forEach { en ->
                        if (en is JsonObject) {
                            entries(en)
                        } else {
                            +en.asString
                            br
                        }
                    }
                }
                "inset" -> {
                    p(classes = "inset") {
                        val arr = it.get("entries").asJsonArray
                        arr.forEach { en ->
                            if (en is JsonObject) {
                                entries(en)
                            } else {
                                +en.asString
                                br
                            }
                        }
                    }
                }
                "options" -> {
                    ul {
                        val arr = it.get("entries").asJsonArray
                        arr.forEach { en ->
                            if (en is JsonObject) {
                                li {
                                    p { entries(en) }
                                }
                            } else {
                                li {
                                    +en.asString
                                }
                            }
                        }
                    }
                }
                "table" -> {
                    if (it.has("caption")) {
                        b { +it.get("caption").asString }
                        br
                    }
                    table {
                        if (it.has("colLabels")) {
                            tr {
                                it.get("colLabels")
                                    .asJsonArray.forEach { lab ->
                                    th { +lab.asString }
                                }
                            }
                        }
                        if (it.has("rows")) {
                            it.get("rows").asJsonArray.forEach { row ->
                                tr {
                                    var rr: JsonArray? = null
                                    if (row is JsonArray) {
                                        rr = row.asJsonArray
                                    }
                                    if (row is JsonObject) {
                                        rr = row.get("row").asJsonArray
                                    }
                                    rr!!.forEach { rx ->
                                        td {
                                            if (rx is JsonObject) {
                                                +makeObjectStr(rx)
                                            } else {
                                                +rx.asString
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                "list" -> {
                    ul {
                        if (it.has("items")) {
                            it.get("items").asJsonArray.forEach { item ->
                                if (item is JsonObject) {
                                    val ito = item.asJsonObject
                                    li {
                                        b {
                                            +ito.get("name").asString
                                            +": "
                                        }
                                        if (ito.has("entry")) {
                                            +ito.get("entry").asString
                                        }
                                        if (ito.has("entries")) {
                                            ito.get("entries").asJsonArray.forEach { ent ->
                                                p {
                                                    entries(ent)
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    li {
                                        +item.asString
                                    }
                                }

                            }
                        }
                    }
                }
                "section" -> {
                    if (it.has("name")) {
                        h1 {
                            +it.get("name").asString
                        }
                    }
                    if (it.has("entries")) {
                        val arr = it.get("entries").asJsonArray
                        arr.forEach { en ->
                            if (en is JsonObject) {
                                p {
                                    entries(en)
                                }
                            } else {
                                p {
                                    +en.asString
                                }
                            }
                        }
                    }
                }
                "abilityDc" -> {
                    +"ability DC: "
                    i {
                        +it.get("attributes").asJsonArray.map { it.asString }.joinToString(", ")
                    }
                    br
                }
                "abilityAttackMod" -> {
                    +"ability attack mod: "
                    i {
                        +it.get("attributes").asJsonArray.map { it.asString }.joinToString(", ")
                    }
                    br
                }
                else -> {
                    +it.toString()
                }
            }
        } else if (it is JsonArray) {
            it.asJsonArray.forEach { tt ->
                p {
                    entries(tt)
                }
            }
        } else {
            try {
                +it.asString
            } catch (e: Exception) {
                +makeObjectStr(it)
            }
        }
    }

    fun DIV.race(obj: JsonObject) {
        if (obj.has("ability")) {
            table {
                thead {
                    tr {
                        th { +"STR" }
                        th { +"DEX" }
                        th { +"CON" }
                        th { +"INT" }
                        th { +"WIS" }
                        th { +"CHA" }
                    }
                }
                tbody {
                    val arr = obj.getAsJsonArray("ability")
                    arr.forEach {
                        tr {
                            td {
                                +makeAbilityTd(it, "str")
                            }
                            td {
                                +makeAbilityTd(it, "dex")
                            }
                            td {
                                +makeAbilityTd(it, "con")
                            }
                            td {
                                +makeAbilityTd(it, "int")
                            }
                            td {
                                +makeAbilityTd(it, "wis")
                            }
                            td {
                                +makeAbilityTd(it, "cha")
                            }
                        }
                    }
                }
            }
        }
        ul {
            if (obj.has("source")) {
                li {
                    b { +"Source: " }
                    +obj.get("source").asString
                }
            }
            if (obj.has("size")) {
                li {
                    b { +"Size: " }
                    +obj.get("size").asString
                }
            }
            if (obj.has("speed")) {
                li {
                    b { +"Speed: " }
                    +makeObjectStr(obj.get("speed"))
                }
            }
            if (obj.has("traitTags")) {
                li {
                    b { +"Trait Tags: " }
                    +makeListString(obj.get("traitTags"))
                }
            }
            if (obj.has("darkvision")) {
                li {
                    b { +"Darkvision: " }
                    +obj.get("darkvision").asString
                }
            }
            if (obj.has("languageProficiencies")) {
                val arr = obj.get("languageProficiencies").asJsonArray
                arr.forEach { sp ->
                    li {
                        b {
                            +"Language Proficiencies: "
                        }
                        +makeProficiencies(sp)
                    }
                }
            }
            if (obj.has("skillProficiencies")) {
                val arr = obj.get("skillProficiencies").asJsonArray
                arr.forEach { sp ->
                    li {
                        b {
                            +"Skill Proficiencies: "
                        }
                        +makeProficiencies(sp)
                    }
                }
            }
        }
        if (obj.has("entries")) {
            val arr = obj.get("entries").asJsonArray
            arr.forEach {
                p {
                    entries(it)
                }

            }
        }
    }

    fun DIV.classTable(obj: JsonObject) {
        if (obj.has("classTableGroups")) {
            val hdr = mutableListOf<String>("Lvl")
            val retv = mutableListOf<MutableList<String>>()
            for (i in 1..20) {
                retv.add(mutableListOf(i.toString()))
            }
            val arr = obj.get("classTableGroups").asJsonArray
            arr.forEach { tb ->
                if (tb.asJsonObject.has("colLabels")) {
                    tb.asJsonObject.get("colLabels").asJsonArray.forEach { lab ->
                        hdr.add(lab.asString)
                    }
                }
                if (tb.asJsonObject.has("rows")) {
                    var cnt = 0
                    tb.asJsonObject.get("rows").asJsonArray.forEach { row ->
                        row.asJsonArray.forEach { vl ->
                            retv[cnt].add(makeObjectStr(vl))
                        }
                        cnt++
                    }
                }
            }
            table {
                tr {
                    hdr.forEach { hd ->
                        th {
                            +hd
                        }
                    }
                }
                retv.forEach { rw ->
                    tr {
                        rw.forEach { rr ->
                            td {
                                +rr
                            }
                        }
                    }
                }
            }
        }

    }

    fun DIV.clazz(obj: JsonObject) {
        ul {
            if (obj.has("source")) {
                li {
                    b { +"Source: " }
                    +obj.get("source").asString
                }
            }
            if (obj.has("hd")) {
                li {
                    b { +"Hit Dice: " }
                    +makeDice(obj.get("hd"))
                }
            }
            if (obj.has("proficiency")) {
                li {
                    b { +"Saving throws: " }
                    +makeObjectStr(obj.get("proficiency"))
                }
            }
            if (obj.has("spellcastingAbility")) {
                li {
                    b { +"Spellcasting ability: " }
                    +makeObjectStr(obj.get("spellcastingAbility"))
                }
            }
            if (obj.has("casterProgression")) {
                li {
                    b { +"Caster Progression: " }
                    +makeObjectStr(obj.get("casterProgression"))
                }
            }
            if (obj.has("startingProficiencies")) {
                li {
                    b { +"Starting Proficiencies: " }
                    +makeObjectStr(obj.get("startingProficiencies"))
                }
            }
            if (obj.has("startingEquipment")) {
                val oo = obj.get("startingEquipment")
                if (oo is JsonObject && oo.has("default")) {
                    li {
                        b {
                            +"Starting equipment: "
                        }
                        +makeObjectStr(oo.get("default"))
                    }
                }
            }
            if (obj.has("subclassTitle")) {
                li {
                    b { +"Subclass title: " }
                    +obj.get("subclassTitle").asString
                }
            }
        }
        if (obj.has("classFeatures")) {
            h3 {
                +"Features"
            }
            obj.get("classFeatures").asJsonArray.forEach { stp ->
                if (stp is JsonArray) {
                    stp.asJsonArray.forEach { aa ->
                        p {
                            entries(aa)
                        }
                    }
                } else {
                    p {
                        entries(stp)
                    }
                }
            }
        }
        if (obj.has("subclassFeatures")) {
            h3 {
                +"Features"
            }
            obj.get("subclassFeatures").asJsonArray.forEach { stp ->
                if (stp is JsonArray) {
                    stp.asJsonArray.forEach { aa ->
                        p {
                            entries(aa)
                        }
                    }
                } else {
                    p {
                        entries(stp)
                    }
                }
            }
        }
        div {
            classTable(obj)
        }

    }


    // ------ RACE
    fun parseRace(obj: JsonObject): JsonData {
        val retv = StringBuilder()
        var title = ""
        retv.appendHTML().html {
            head {
                refstyle(PrefUtil.isNightMode())
            }
            body {
                div {
                    div(classes = "center") {
                        h1 {
                            title = obj.get("name").asString
                            +obj.get("name").asString
                        }
                    }
                    race(obj)
                    if (obj.has("subraces")) {
                        h2 {
                            +"Subraces"
                        }
                        obj.get("subraces").asJsonArray.forEach { sub ->
                            if (sub.asJsonObject.has("name")) {
                                div(classes = "center") {
                                    h3 {
                                        +sub.asJsonObject.get("name").asString
                                    }
                                }
                            }
                            race(sub.asJsonObject)
                        }
                    }
                }
            }
        }
        return JsonData(JsonDataType.RACE, filterAts(retv.toString()), title)
    }

    // ------ BACKGROUND
    fun parseBackground(obj: JsonObject): JsonData {
        var retv = StringBuilder()
        var title = ""
        retv.appendHTML().html {
            head { refstyle(PrefUtil.isNightMode()) }
            body {
                div {
                    div(classes = "center") {
                        h1 {
                            title = obj.get("name").asString
                            +obj.get("name").asString
                        }
                    }
                    ul {
                        if (obj.has("source")) {
                            li {
                                b { +"Source: " }
                                +obj.get("source").asString
                            }
                        }
                        if (obj.has("skillProficiencies")) {
                            val arr = obj.get("skillProficiencies").asJsonArray
                            arr.forEach { sp ->
                                li {
                                    b {
                                        +"Skill Proficiencies: "
                                    }
                                    +makeProficiencies(sp)
                                }
                            }
                        }
                        if (obj.has("languageProficiencies")) {
                            val arr = obj.get("languageProficiencies").asJsonArray
                            arr.forEach { sp ->
                                li {
                                    b {
                                        +"Language Proficiencies: "
                                    }
                                    +makeProficiencies(sp)
                                }
                            }
                        }
                    }
                    if (obj.has("entries")) {
                        val arr = obj.get("entries").asJsonArray
                        arr.forEach {
                            p {
                                entries(it)
                            }

                        }
                    }
                }
            }
        }
        return JsonData(JsonDataType.BACKGROUND, filterAts(retv.toString()), title)
    }

    // ------ RULES
    fun parseRules(obj: JsonObject): List<JsonData> {
        val retl = mutableListOf<JsonData>()
        if (obj.has("entries")) {
            obj.get("entries").asJsonArray.forEach { en ->
                var retv = StringBuilder()
                var title = ""
                if (en is JsonObject && en.has("name")) {
                    title = en.get("name").asString
                }
                retv.appendHTML().html {
                    head { refstyle(PrefUtil.isNightMode()) }
                    body {
                        p {
                            entries(en, false)
                        }
                    }
                }
                retl.add(JsonData(JsonDataType.RULES, filterAts(retv.toString()), title))
            }
        }
        return retl
    }

    // ------ CONDITIONS
    fun parseCondition(obj: JsonObject): JsonData {
        var retv = StringBuilder()
        var title = ""
        if (obj is JsonObject && obj.has("name")) {
            title = obj.get("name").asString
        }
        obj.addProperty("type", "entries")
        retv.appendHTML().html {
            head { refstyle(PrefUtil.isNightMode()) }
            body {
                h1 {
                    +obj.get("name").asString
                }
                p {
                    entries(obj, false)
                }
            }
        }
        return JsonData(JsonDataType.CONDITION, filterAts(retv.toString()), title)
    }

    // ------ FEAT
    fun parseFeat(obj: JsonObject): JsonData {
        var retv = StringBuilder()
        var title = ""
        if (obj is JsonObject && obj.has("name")) {
            title = obj.get("name").asString
        }
        obj.addProperty("type", "entries")
        retv.appendHTML().html {
            head { refstyle(PrefUtil.isNightMode()) }
            body {
                div {
                    h1 {
                        +obj.get("name").asString
                    }
                    ul {
                        if (obj.has("prerequisite")) {
                            val arr = obj.get("prerequisite").asJsonArray
                            val lst = mutableListOf<String>()
                            arr.forEach {
                                lst.add(makeObjectStr(it))
                            }
                            li {
                                b {
                                    +"Prerequisite: "
                                }
                                +lst.toList().joinToString(", ")
                            }
                        }
                        if (obj.has("ability")) {
                            val arr = obj.get("ability").asJsonArray
                            val lst = mutableListOf<String>()
                            arr.forEach {
                                lst.add(makeProficiencies(it))
                            }
                            li {
                                b {
                                    +"Ability: "
                                }
                                +lst.toList().joinToString(", ")
                            }
                        }
                    }
                    p {
                        entries(obj, false)
                    }
                }
            }
        }
        return JsonData(JsonDataType.FEAT, filterAts(retv.toString()), title)
    }

    // ------ BEAST
    fun parseBeast(obj: JsonObject): JsonData {
        var retv = StringBuilder()
        var title = ""
        if (obj is JsonObject && obj.has("name")) {
            title = obj.get("name").asString
        }
        retv.appendHTML().html {
            head { refstyle(PrefUtil.isNightMode()) }
            body {
                div {
                    h1 {
                        +obj.get("name").asString
                    }
                    table {
                        thead {
                            tr {
                                th { +"STR" }
                                th { +"DEX" }
                                th { +"CON" }
                                th { +"INT" }
                                th { +"WIS" }
                                th { +"CHA" }
                            }
                        }
                        tbody {
                            tr {
                                td {
                                    +makeAbilityTd(obj, "str")
                                }
                                td {
                                    +makeAbilityTd(obj, "dex")
                                }
                                td {
                                    +makeAbilityTd(obj, "con")
                                }
                                td {
                                    +makeAbilityTd(obj, "int")
                                }
                                td {
                                    +makeAbilityTd(obj, "wis")
                                }
                                td {
                                    +makeAbilityTd(obj, "cha")
                                }
                            }
                        }
                    }
                    ul {
                        if (obj.has("source")) {
                            li {
                                b { +"Source: " }
                                +obj.get("source").asString
                            }
                        }
                        if (obj.has("type")) {
                            li {
                                b { +"Type: " }
                                +makeObjectStr(obj.get("type"))
                            }
                        }
                        if (obj.has("size")) {
                            li {
                                b { +"Size: " }
                                +obj.get("size").asString
                            }
                        }
                        if (obj.has("alignment")) {
                            li {
                                b { +"Alignment: " }
                                +makeObjectStr(obj.get("alignment"))
                            }
                        }
                        if (obj.has("ac")) {
                            li {
                                b { +"AC: " }
                                +makeObjectStr(obj.get("ac"))
                            }
                        }
                        if (obj.has("hp")) {
                            li {
                                b { +"HP: " }
                                +makeObjectStr(obj.get("hp"))
                            }
                        }
                        if (obj.has("speed")) {
                            li {
                                b { +"Speed: " }
                                +makeObjectStr(obj.get("speed"))
                            }
                        }
                        if (obj.has("skill")) {
                            li {
                                b { +"Skill: " }
                                +makeObjectStr(obj.get("skill"))
                            }
                        }
                        if (obj.has("senses")) {
                            li {
                                b { +"Senses: " }
                                +makeObjectStr(obj.get("senses"))
                            }
                        }
                        if (obj.has("passive")) {
                            li {
                                b { +"Passive perception: " }
                                +makeObjectStr(obj.get("passive"))
                            }
                        }
                    }
                    if (obj.has("trait")) {
                        h3 {
                            +"Traits"
                        }
                        val arr = obj.get("trait").asJsonArray
                        arr.forEach { tt ->
                            tt.asJsonObject.addProperty("type", "entries")
                            p {
                                entries(tt)
                            }
                        }
                    }
                    if (obj.has("action")) {
                        h3 {
                            +"Actions"
                        }
                        val arr = obj.get("action").asJsonArray
                        arr.forEach { tt ->
                            tt.asJsonObject.addProperty("type", "entries")
                            p {
                                entries(tt)
                            }
                        }
                    }
                }
            }
        }

        return JsonData(JsonDataType.BEAST, filterAts(retv.toString()), title)
    }

    // ------ CLASS
    fun parseClass(obj: JsonObject): JsonData {
        var retv = StringBuilder()
        var title = ""
        if (obj is JsonObject && obj.has("name")) {
            title = obj.get("name").asString
        }
        retv.appendHTML().html {
            head { refstyle(PrefUtil.isNightMode()) }
            body {
                div {
                    div(classes = "center") {
                        h1 {
                            +obj.get("name").asString
                        }
                    }
                    clazz(obj)
                    if (obj.has("subclasses")) {
                        h2 {
                            +"Subclasses"
                        }
                        val arr = obj.get("subclasses").asJsonArray
                        arr.forEach {
                            div {
                                if (it.asJsonObject.has("name")) {
                                    div(classes = "center") {
                                        h3 {
                                            +it.asJsonObject.get("name").asString
                                        }
                                    }
                                }
                                clazz(it.asJsonObject)
                            }
                        }
                    }
                }
            }
        }
        return JsonData(JsonDataType.CLASS, filterAts(retv.toString()), title)
    }

    // ------ ITEM
    fun parseItem(obj: JsonObject): JsonData {
        var retv = StringBuilder()
        var title = ""
        if (obj is JsonObject && obj.has("name")) {
            title = obj.get("name").asString
        }
        retv.appendHTML().html {
            head { refstyle(PrefUtil.isNightMode()) }
            body {
                div {
                    div(classes = "center") {
                        h1 {
                            +obj.get("name").asString
                        }
                    }
                    ul {
                        if (obj.has("rarity")) {
                            li {
                                b { +"Rarity: " }
                                +obj.get("rarity").asString
                            }
                        }
                        if (obj.has("type")) {
                            li {
                                b { +"Type: " }
                                +obj.get("type").asString
                            }
                        }
                        if (obj.has("value")) {
                            li {
                                b { +"Value: " }
                                +obj.get("value").asString
                            }
                        }
                        if (obj.has("weight")) {
                            li {
                                b { +"Weight: " }
                                +obj.get("weight").asString
                            }
                        }
                        if (obj.has("crew")) {
                            li {
                                b { +"Crew: " }
                                +obj.get("crew").asString
                            }
                        }
                        if (obj.has("vehAc")) {
                            li {
                                b { +"Vehicle AC: " }
                                +obj.get("vehAc").asString
                            }
                        }
                        if (obj.has("vehHp")) {
                            li {
                                b { +"Vehicle HP: " }
                                +obj.get("vehHp").asString
                            }
                        }
                        if (obj.has("vehSpeed")) {
                            li {
                                b { +"Vehicle Speed: " }
                                +obj.get("vehSpeed").asString
                            }
                        }
                    }
                    if (obj.has("entries")) {
                        p {
                            entries(obj.get("entries"))
                        }
                    }
                    if (obj.has("additionalEntries")) {
                        p {
                            entries(obj.get("additionalEntries"))
                        }
                    }
                    if (obj.has("items")) {
                        ul {
                            obj.get("items").asJsonArray.forEach {
                                li {
                                    +makeObjectStr(it)
                                }
                            }
                        }
                    }
                }
            }
        }
        return JsonData(JsonDataType.ITEM, filterAts(retv.toString()), title)
    }

    // ------ PSIONICS
    fun parsePsionics(obj: JsonObject): JsonData {
        var retv = StringBuilder()
        var title = ""
        if (obj is JsonObject && obj.has("name")) {
            title = obj.get("name").asString
        }
        obj.addProperty("ptype", obj.get("type").asString)
        obj.addProperty("type", "entries")
        retv.appendHTML().html {
            head { refstyle(PrefUtil.isNightMode()) }
            body {
                h1 {
                    +obj.get("name").asString
                }
                if (obj.has("entries")) {
                    p {
                        entries(obj, false)
                    }
                } else if (obj.has("modes")) {
                    obj.get("modes").asJsonArray.forEach { md ->
                        p {
                            ul {
                                if (md.asJsonObject.has("cost")) {
                                    li {
                                        b { +"Cost: " }
                                        +makeObjectStr(md.asJsonObject.get("cost"))
                                    }
                                }
                                if (md.asJsonObject.has("concentration")) {
                                    li {
                                        b { +"Concentration: " }
                                        +makeObjectStr(md.asJsonObject.get("concentration"))
                                    }
                                }
                            }
                            entries(md)
                        }
                    }
                }
            }
        }
        return JsonData(JsonDataType.PSIONICS, filterAts(retv.toString()), title)
    }


    fun UL.strli(it: JsonElement, name: String, title: String) {
        if (it is JsonObject && it.has(name)) {
            li {
                b {
                    +"$title: "
                }
                +makeObjectStr(it.get(name))
            }
        }
    }

    fun makeTime(it: JsonElement): String {
        when (it) {
            is JsonObject -> {
                var nm = ""
                var un = ""
                var cnd = ""
                if (it.has("number")) {
                    nm = it.get("number").asString
                }
                if (it.has("unit")) {
                    un = it.get("unit").asString
                }
                if (it.has("condition")) {
                    cnd = """(${it.get("condition").asString})"""
                }
                return "$nm $un $cnd"
            }
            is JsonArray -> {
                val lst = mutableListOf<String>()
                it.asJsonArray.forEach { aa ->
                    lst.add(makeTime(aa))
                }
                return lst.joinToString(", ")
            }
            else -> {
                return it.toString()
            }
        }
    }

    fun makeRange(it: JsonElement): String {
        if (it is JsonObject) {
            if (it.has("distance")) {
                val xx = it.get("distance").asJsonObject
                var tp = ""
                var amnt = ""
                if (xx.has("type")) {
                    tp = xx.get("type").asString
                }
                if (xx.has("amount")) {
                    amnt = xx.get("amount").asString
                }
                return "distance: $amnt $tp"
            } else {
                return makeObjectStr(it)
            }
        } else {
            return it.toString()
        }
        return "???"
    }

    fun makeDuration(it: JsonElement): String {
        when (it) {
            is JsonObject -> {
                var tp = ""
                if (it.has("type")) {
                    tp = it.get("type").asString
                }
                var dt = ""
                if (it.has("duration")) {
                    dt = makeObjectStr(it.get("duration"))
                }
                return "$tp: $dt"
            }
            is JsonArray -> {
                val lst = mutableListOf<String>()
                it.asJsonArray.forEach { aa ->
                    lst.add(makeDuration(aa))
                }
                return lst.joinToString(", ")
            }
            else -> {
                return it.toString()
            }
        }
    }

    fun makeSpellClasses(it: JsonElement): String {
        if (it is JsonObject) {
            val lst = mutableListOf<String>()
            if (it.has("fromClassList")) {
                val arr = it.get("fromClassList").asJsonArray
                arr.forEach { cc ->
                    lst.add(cc.asJsonObject.get("name").asString)
                }
            }
            if (it.has("fromClassListVariant")) {
                val arr = it.get("fromClassListVariant").asJsonArray
                arr.forEach { cc ->
                    lst.add(cc.asJsonObject.get("name").asString)
                }
            }
            if (it.has("fromSubclass")) {
                val arr = it.get("fromSubclass").asJsonArray
                arr.forEach { cc ->
                    lst.add(
                        cc.asJsonObject.get("class").asJsonObject.get("name").asString
                                + " "
                                + cc.asJsonObject.get("subclass").asJsonObject.get("name").asString
                    )
                }
            }
            return lst.joinToString(", ")
        }
        return it.toString()
    }

    // ------ SPELLS
    fun parseSpell(obj: JsonObject): JsonData {
        var retv = StringBuilder()
        var title = ""
        if (obj.has("name")) {
            title = obj.get("name").asString
        }
        retv.appendHTML().html {
            head { refstyle(PrefUtil.isNightMode()) }
            body {
                div(classes = "center") {
                    h1 {
                        +obj.get("name").asString
                    }
                }
                ul {
                    strli(obj, "level", "Level")
                    strli(obj, "school", "School")
                    if (obj.has("time")) {
                        li {
                            b { +"Time: " }
                            +makeTime(obj.get("time"))
                        }
                    }
                    if (obj.has("range")) {
                        li {
                            b { +"Range: " }
                            +makeRange(obj.get("range"))
                        }
                    }
                    strli(obj, "components", "Components")
                    if (obj.has("duration")) {
                        li {
                            b { +"Duration: " }
                            +makeDuration(obj.get("duration"))
                        }
                    }
                    if (obj.has("classes")) {
                        li {
                            b { +"Classes: " }
                            +makeSpellClasses(obj.get("classes"))
                        }
                    }
                    strli(obj, "savingThrow", "Saving Throw")
                    strli(obj, "conditionInflict", "Condition Inflict")
                    strli(obj, "damageInflict", "Damage Inflict")
                    if (obj.has("races")) {
                        val lst = mutableListOf<String>()
                        obj.get("races").asJsonArray.forEach {
                            lst.add(it.asJsonObject.get("name").asString)
                        }
                        li {
                            b { +"Races: " }
                            +lst.joinToString(", ")
                        }
                    }
                }
                if (obj.has("entries")) {
                    obj.get("entries").asJsonArray.forEach {
                        p {
                            entries(it)
                        }
                    }
                }
                if (obj.has("entriesHigherLevel")) {
                    obj.get("entriesHigherLevel").asJsonArray.forEach {
                        p {
                            entries(it)
                        }
                    }
                }
            }
        }
        return JsonData(JsonDataType.SPELL, filterAts(retv.toString()), title)
    }

    // ---- Infos

    fun findInfo(context: Context, name: String, viewType: JsonDataType): String {
        var filename = ""
        when (viewType) {
            JsonDataType.RACE -> filename = "json/races.json"
            JsonDataType.BACKGROUND -> filename = "json/backgrounds.json"
            JsonDataType.RULES -> filename = "json/reference.json"
            JsonDataType.CLASS -> filename = "json/classes.json"
            JsonDataType.CONDITION -> filename = "json/conditions.json"
            JsonDataType.FEAT -> filename = "json/feats.json"
            JsonDataType.BEAST -> filename = "json/bestiary.json"
            JsonDataType.ITEM -> filename = "json/items.json"
            JsonDataType.PSIONICS -> filename = "json/psionics.json"
            JsonDataType.SPELL -> filename = "json/spells.json"
            JsonDataType.PLANESCAPE_BEAST -> filename = "json/planescape_bestiary.json"
        }
        val data = FileUtil.assetFile(context, filename)
        val listType: Type = object : TypeToken<List<JsonObject?>?>() {}.type
        var retv = ""
        try {
            val lst: List<JsonObject> = Gson().fromJson(data, listType)
            for(i in lst.indices){
                val it = lst[i]
                val nm = it.asJsonObject.get("name").asString.toLowerCase(Locale.ROOT)
                if ( nm == name.toLowerCase(Locale.ROOT) || nm.startsWith(name.toLowerCase(Locale.ROOT)) ) {
                    retv +=  when (viewType) {
                        JsonDataType.RACE -> parseRace(it).data
                        JsonDataType.BACKGROUND -> parseBackground(it).data
                        JsonDataType.CONDITION -> parseCondition(it).data
                        JsonDataType.FEAT -> parseFeat(it).data
                        JsonDataType.BEAST -> parseBeast(it).data
                        JsonDataType.CLASS -> parseClass(it).data
                        JsonDataType.ITEM -> parseItem(it).data
                        JsonDataType.PSIONICS -> parsePsionics(it).data
                        JsonDataType.SPELL -> parseSpell(it).data
                        JsonDataType.PLANESCAPE_BEAST -> parseBeast(it).data
                        else -> "Not found"
                    }
                }
            }
        } catch (ex:Exception) {
            return "Not found"
        }
        return retv
    }
}