package osw.cage.ui.ref

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import kotlinx.android.synthetic.main.app_bar_main.*
import osw.cage.R
import osw.cage.db.DbWorker
import osw.cage.db.FAV_MAP
import osw.cage.db.FAV_REF
import osw.cage.db.Favourite
import osw.cage.util.DlgUtil
import osw.cage.util.PrefUtil

class RefListFragment : Fragment() {

    lateinit var recyclerView: RecyclerView
    private lateinit var refViewModel: RefViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_reflist, container, false)
        recyclerView = root.findViewById(R.id.listView)
        refViewModel =
            ViewModelProviders.of(this).get(RefViewModel::class.java)
        val itemAdapter = ItemAdapter<RefItem>()
        val fastAdapter = FastAdapter.with(itemAdapter)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = fastAdapter
        refViewModel.list.observe(this, Observer {
            itemAdapter.add(it)
        })
        fastAdapter.onClickListener = { view, adapter, item, position ->
            if (item.dm) {
                DlgUtil.inputPass(context!!, "DM Access", "Enter DM password", false) { pass, _ ->
                    if (pass == PrefUtil.getDmPassword()) {
                        openRef(item)
                    }
                }
            } else {
                openRef(item)
            }
            false
        }
        fastAdapter.onLongClickListener = { _, _, item, _ ->
            DlgUtil.confirm(context!!, "Add favourite", "Please confirm adding ${item.name}", {
                DbWorker.postDbTask(context) { db ->
                    db.favouriteDao().insertAll(
                        Favourite(
                            null,
                            item.name,
                            FAV_REF,
                            item.itemType.toString(),
                            "",
                            item.dm
                        )
                    )
                    Toast.makeText(context, "${item.name} added to favourites", Toast.LENGTH_SHORT)
                        .show()
                }
            }, {})
            false
        }
        activity?.toolbar?.title = "Reference"
        return root
    }

    fun openRef(item: RefItem) {
        val tr = fragmentManager!!.beginTransaction()
        tr.replace(R.id.nav_host_fragment, RefFragment(item.itemType))
        tr.addToBackStack(null)
        tr.commit()
    }
}