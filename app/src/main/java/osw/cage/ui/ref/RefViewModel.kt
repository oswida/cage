package osw.cage.ui.ref

import android.content.Context
import android.net.Uri
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.viewpager.widget.PagerAdapter
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import osw.cage.R
import osw.cage.util.DlgUtil
import osw.cage.util.RefClient


enum class JsonDataType {
    BACKGROUND,
    RACE,
    RULES,
    CLASS,
    CONDITION,
    FEAT,
    BEAST,
    ITEM,
    PSIONICS,
    SPELL,
    PLANESCAPE_BEAST
}

class JsonData(val type: JsonDataType, val data: String, val title: String)

private val mClient = RefClient()

class RefAdapter(val list: List<JsonData>, val context: Context) : PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return list.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val webView = WebView(context)
        webView.webViewClient = mClient
        webView.settings.javaScriptEnabled = true
        webView.loadDataWithBaseURL(
            "",
            list[position].data,
            "text/html", "utf-8", ""
        )
        container.addView(webView)
        return webView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {

    }

}

open class RefItem(
    var name: String? = null,
    var description: String? = null,
    var itemType: JsonDataType,
    var dm: Boolean = false
) : AbstractItem<RefItem.ViewHolder>() {

    override val type: Int
        get() = R.id.jsonDataItem

    override val layoutRes: Int
        get() = R.layout.list_item

    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v)
    }

    class ViewHolder(view: View) : FastAdapter.ViewHolder<RefItem>(view) {
        var name: TextView = view.findViewById(R.id.title)
        var description: TextView = view.findViewById(R.id.subtitle)
        var icon: ImageView = view.findViewById(R.id.icon)

        override fun bindView(item: RefItem, payloads: MutableList<Any>) {
            name.text = item.name
            description.text = item.description
            if (item.dm) {
                icon.setImageResource(R.drawable.ic_password)
            }
        }

        override fun unbindView(item: RefItem) {
            name.text = null
            description.text = null
        }
    }
}

class RefViewModel : ViewModel() {

    private val _list = MutableLiveData<List<RefItem>>().apply {
        value = listOf(
            RefItem("Backgrounds", "Character backgrounds", JsonDataType.BACKGROUND),
            RefItem("Bestiary", "Comprehensive bestiary", JsonDataType.BEAST, true),
            RefItem("Classes", "Character classes", JsonDataType.CLASS),
            RefItem("Conditions", "Situational conditions", JsonDataType.CONDITION),
            RefItem("Features", "Character features", JsonDataType.FEAT),
            RefItem("Items", "Misc items", JsonDataType.ITEM),
            RefItem("Psionics", "Psionics powers", JsonDataType.PSIONICS),
            RefItem("Races", "Character races", JsonDataType.RACE),
            RefItem("Spells", "Spell list", JsonDataType.SPELL),
            RefItem("Quick rules", "Quick rules reference", JsonDataType.RULES),
            RefItem(
                "Planescape bestiary",
                "Specific to Planescape",
                JsonDataType.PLANESCAPE_BEAST,
                true
            )
        )
    }
    val list: LiveData<List<RefItem>> = _list

}
