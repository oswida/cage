package osw.cage.ui.tools

import android.view.View
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import osw.cage.R

open class ToolsItem(
    var name: String? = null,
    var description: String? = null
) : AbstractItem<ToolsItem.ViewHolder>() {

    override val type: Int
        get() = R.id.toolslist

    override val layoutRes: Int
        get() = R.layout.list_item

    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v)
    }

    class ViewHolder(view: View) : FastAdapter.ViewHolder<ToolsItem>(view) {
        var name: TextView = view.findViewById(R.id.title)
        var description: TextView = view.findViewById(R.id.subtitle)

        override fun bindView(item: ToolsItem, payloads: MutableList<Any>) {
            name.text = item.name
            description.text = item.description
        }

        override fun unbindView(item: ToolsItem) {
            name.text = null
            description.text = null
        }
    }
}

class ToolsViewModel : ViewModel() {

    private val _list = MutableLiveData<List<ToolsItem>>().apply {
        value = listOf(
            ToolsItem(
                "Combat tracker", "Planned encounters tracking tool"
            ),
            ToolsItem(
                "Encounter planner", "Encounter planner for DM"
            ),
            ToolsItem(
                "Combat entry list", "List of predefined entries for encounter planner"
            )
        )
    }
    val list: LiveData<List<ToolsItem>> = _list
}