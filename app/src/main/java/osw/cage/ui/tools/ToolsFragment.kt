package osw.cage.ui.tools

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import kotlinx.android.synthetic.main.app_bar_main.*
import osw.cage.R
import osw.cage.ui.track.CombatEntryListFragment

class ToolsFragment : Fragment() {

    lateinit var recyclerView: RecyclerView
    private lateinit var toolsViewModel: ToolsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        toolsViewModel =
            ViewModelProviders.of(this).get(ToolsViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_tools, container, false)
        recyclerView = root.findViewById(R.id.listView)
        toolsViewModel =
            ViewModelProviders.of(this).get(ToolsViewModel::class.java)
        val itemAdapter = ItemAdapter<ToolsItem>()
        val fastAdapter = FastAdapter.with(itemAdapter)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = fastAdapter
        toolsViewModel.list.observe(this, Observer {
            itemAdapter.add(it)
        })
        fastAdapter.onClickListener = { view, adapter, item, position ->
            when (position) {
                0 -> {

                }
                1 -> {

                }
                2 -> {
                    val tr = fragmentManager!!.beginTransaction()
                    tr.replace(R.id.nav_host_fragment, CombatEntryListFragment())
                    tr.addToBackStack(null)
                    tr.commit()
                }
            }

            false
        }
        activity?.toolbar?.title = "Tools"
        return root
    }


}