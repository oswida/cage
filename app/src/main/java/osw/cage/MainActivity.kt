package osw.cage

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import osw.cage.db.AppDatabase
import osw.cage.db.DbWorker
import osw.cage.util.PrefUtil

class MainActivity : AppCompatActivity() {

    companion object {
        const val MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 150
    }

    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)

        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_ref,
                R.id.nav_pdf,
                R.id.nav_maps,
                R.id.nav_cards,
                R.id.nav_settings,
                R.id.nav_fav
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        PrefUtil.initialize(this)
        if (PrefUtil.getDmPassword().isNullOrEmpty()) {
            PrefUtil.setDmPassword("alamakota72")
        }
        switchNightmode()
        DbWorker.getInstance()!!.start()
        checkPermissions()
        openStartPage()
    }

    fun switchNightmode() {
        AppCompatDelegate.setDefaultNightMode(PrefUtil.getNightmode())
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onDestroy() {
        DbWorker.getInstance()?.quit()
        DbWorker.destroyInstance()
        AppDatabase.getInstance(this)?.close()
        AppDatabase.destroyInstance()
        super.onDestroy()
    }

    fun openStartPage() {
        val sp = PrefUtil.getStartingPage()
        val navController = findNavController(R.id.nav_host_fragment)
        when (sp) {
            "Reference" -> {
                navController.navigate(R.id.nav_ref)
            }
            "PDF Documents" -> {
                navController.navigate(R.id.nav_pdf)
            }
            "Maps" -> {
                navController.navigate(R.id.nav_maps)
            }
            "Cards" -> {
                navController.navigate(R.id.nav_cards)
            }
            else -> {
            }
        }
    }

    fun checkPermissions() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
            ) {
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE
                )
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show()
                }
                return
            }
            else -> {
                // Ignore all other requests.
            }
        }
    }

}
