package osw.cage.db

import androidx.room.*


@Entity(tableName = "cardgroup")
data class CardGroup(
    @PrimaryKey(autoGenerate = true) var id: Long?,
    @ColumnInfo(name = "name") var title: String?,
    @ColumnInfo(name = "description") var description: String?
)

@Dao
interface CardGroupDao {

    @Query("SELECT * FROM cardgroup WHERE name LIKE :filter")
    fun getAll(filter: String): List<CardGroup>

    @Query("SELECT * FROM cardgroup")
    fun getAll(): List<CardGroup>

    @Query("SELECT * FROM cardgroup WHERE name=:title")
    fun findByName(title: String): CardGroup?

    @Insert
    fun insertAll(vararg items: CardGroup)

    @Delete
    fun delete(item: CardGroup)

    @Query("DELETE FROM cardgroup")
    fun clear()

    @Update
    fun save(item: CardGroup)
}

