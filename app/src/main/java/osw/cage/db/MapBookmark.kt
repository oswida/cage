package osw.cage.db

import androidx.room.*

@Entity(tableName = "mapbookmark")
data class MapBookmark(
    @PrimaryKey(autoGenerate = true) var id: Long?,
    @ColumnInfo(name = "path") var path: String?,
    @ColumnInfo(name = "name") var name: String?,
    @ColumnInfo(name = "data") var data: String?
)

@Dao
interface MapBookmarkDao {

    @Query("SELECT * FROM mapbookmark")
    fun getAll(): List<MapBookmark>

    @Query("SELECT * FROM mapbookmark WHERE path=:path")
    fun getAll(path: String): List<MapBookmark>

    @Query("SELECT count(*) FROM mapbookmark WHERE path=:path AND name=:name")
    fun findForName(path: String, name: String): Int

    @Query("SELECT * FROM mapbookmark WHERE path=:path AND name=:name")
    fun getForName(path: String, name: String): MapBookmark

    @Insert
    fun insertAll(vararg items: MapBookmark)

    @Delete
    fun delete(item: MapBookmark)

    @Query("DELETE FROM mapbookmark WHERE path=:path")
    fun deleteForPath(path: String)

    @Query("DELETE FROM mapbookmark WHERE id=:id")
    fun deleteById(id: Long)

    @Query("DELETE FROM mapbookmark")
    fun clear()

    @Update
    fun save(item: MapBookmark)
}