package osw.cage.db

import androidx.room.*

const val FAV_MAP = "Map"
const val FAV_REF = "Reference"
const val FAV_PDF = "PDF Document"
const val FAV_CARDGRP = "Card group"

@Entity(tableName = "favourite")
data class Favourite(
    @PrimaryKey(autoGenerate = true) var id: Long?,
    @ColumnInfo(name = "title") var title: String?,
    @ColumnInfo(name = "doctype") var doctype: String?,
    @ColumnInfo(name = "data1") var data1: String?,
    @ColumnInfo(name = "data2") var data2: String?,
    @ColumnInfo(name = "dm") var dm: Boolean
)

@Dao
interface FavouriteDao {

    @Query("SELECT * FROM favourite WHERE title LIKE :filter")
    fun getAll(filter: String): List<Favourite>

    @Query("SELECT * FROM favourite")
    fun getAll(): List<Favourite>

    @Query("SELECT * FROM favourite WHERE doctype=:doctype")
    fun findForType(doctype: String): Favourite

    @Insert
    fun insertAll(vararg items: Favourite)

    @Delete
    fun delete(item: Favourite)

    @Query("DELETE FROM favourite")
    fun clear()

    @Update
    fun save(item: Favourite)
}

