package osw.cage.db

import androidx.room.*


@Entity(tableName = "combate")
data class CombatEntry(
    @PrimaryKey(autoGenerate = true) var id: Long?,
    @ColumnInfo(name = "name") var title: String?,
    @ColumnInfo(name = "inv") var inv: String?,
    @ColumnInfo(name = "ac") var ac: String?,
    @ColumnInfo(name = "hp") var hp: String?,
    @ColumnInfo(name = "passivePerception") var passivePerception: String?,
    @ColumnInfo(name = "spellSaveDC") var spellSaveDC: String?,
    @ColumnInfo(name = "advantages") var advantages: String?,
    @ColumnInfo(name = "disadvantages") var disadvantages: String?,
    @ColumnInfo(name = "notes") var notes: String?,
    @ColumnInfo(name = "monster") var monster: Boolean,
    @ColumnInfo(name = "json") var json: String?
)

@Dao
interface CombatEntryDao {

    @Query("SELECT * FROM combate WHERE name LIKE :filter")
    fun getAll(filter: String): List<CombatEntry>

    @Query("SELECT * FROM combate")
    fun getAll(): List<CombatEntry>

    @Insert
    fun insertAll(vararg items: CombatEntry)

    @Delete
    fun delete(item: CombatEntry)

    @Query("DELETE FROM combate")
    fun clear()

    @Update
    fun save(item: CombatEntry)
}

