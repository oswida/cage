package osw.cage.db

import androidx.room.*

@Entity(tableName = "pdfbookmark")
data class PdfBookmark(
    @PrimaryKey(autoGenerate = true) var id: Long?,
    @ColumnInfo(name = "path") var path: String?,
    @ColumnInfo(name = "name") var name: String?,
    @ColumnInfo(name = "page") var page: Int?
)

@Dao
interface PdfBookmarkDao {

    @Query("SELECT * FROM pdfbookmark")
    fun getAll(): List<PdfBookmark>

    @Query("SELECT * FROM pdfbookmark WHERE path=:path")
    fun getAll(path: String): List<PdfBookmark>

    @Query("SELECT count(*) FROM pdfbookmark WHERE path=:path AND page=:page")
    fun findForPage(path: String, page: Int): Int

    @Query("SELECT * FROM pdfbookmark WHERE path=:path AND page=:page")
    fun getForPage(path: String, page: Int): PdfBookmark

    @Insert
    fun insertAll(vararg items: PdfBookmark)

    @Delete
    fun delete(item: PdfBookmark)

    @Query("DELETE FROM pdfbookmark WHERE path=:path")
    fun deleteForPath(path: String)

    @Query("DELETE FROM pdfbookmark WHERE id=:id")
    fun deleteById(id: Long)

    @Query("DELETE FROM pdfbookmark")
    fun clear()

    @Update
    fun save(item: PdfBookmark)
}