package osw.cage.db

import androidx.room.*
import androidx.room.ForeignKey.CASCADE

const val CT_JPG = "jpg"
const val CT_PNG = "png"

@Entity(tableName = "card")
@ForeignKey(
    entity = CardGroup::class,
    parentColumns = ["id"],
    childColumns = ["groupId"],
    onDelete = CASCADE
)
data class Card(
    @PrimaryKey(autoGenerate = true) var id: Long?,
    @ColumnInfo(name = "name") var title: String?,
    @ColumnInfo(name = "type") var type: String?,
    @ColumnInfo(name = "path") var path: String?,
    @ColumnInfo(name = "position") var position: Int = 0,
    @ColumnInfo(name = "groupId") var groupId: Long
)

@Dao
interface CardDao {

    @Query("SELECT * FROM card WHERE name LIKE :filter")
    fun getAll(filter: String): List<Card>

    @Query("SELECT * FROM card WHERE groupId=:grpId")
    fun getAllForGroup(grpId: Long): List<Card>

    @Query("SELECT count(*) FROM card WHERE path=:path")
    fun countForPath(path: String): Long

    @Query("SELECT * FROM card")
    fun getAll(): List<Card>

    @Insert
    fun insertAll(vararg items: Card)

    @Delete
    fun delete(item: Card)

    @Query("DELETE FROM card")
    fun clear()

    @Update
    fun save(item: Card)
}

