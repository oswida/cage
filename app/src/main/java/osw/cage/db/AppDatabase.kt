package osw.cage.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

/**
 * Application database.
 */
@Database(
    entities = [
        PdfBookmark::class,
        MapBookmark::class,
        Encounter::class,
        Favourite::class,
        CombatEntry::class,
        CardGroup::class,
        Card::class
    ],
    version = 10
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun pdfBookmarkDao(): PdfBookmarkDao
    abstract fun mapBookmarkDao(): MapBookmarkDao
    abstract fun encounterDao(): EncounterDao
    abstract fun favouriteDao(): FavouriteDao
    abstract fun combateDao(): CombatEntryDao
    abstract fun cardgroupDao(): CardGroupDao
    abstract fun cardDao(): CardDao

    companion object {
        var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java, "cage.db"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }

    }
}


